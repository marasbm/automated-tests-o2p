package paymentpageauthorization;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;
import org.json.JSONObject;

import static com.jayway.restassured.RestAssured.given;

public class PaymentPageAuthorizationRequest {

    private ConfigurationsProperties configurationsProperties;

    public PaymentPageAuthorizationRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response paymentPageAuthorization(String authorizationKey, String authorizationValue, JSONObject jsonObject) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(jsonObject.toString());

        return httpRequest.post(configurationsProperties.paymentPages());
    }

    public JSONObject jsonObjectPaymentPageAuthorizationCompleteRequestBody(){
        return new JSONObject()
                .put("amount",10000)
                .put("orderId","authorizarion-order")
                .put("currency","EUR")
                .put("returnUrl","https://your_domain.com/return")
                .put("responseUrl","https://your_domain.com/confirm")
                .put("usePaymentMeanToken",true)
                .put("customerId","user-1234");
    }

    public JSONObject jsonObjectPaymentPageAuthorizationOnlyAmountRequestBody(){
        return new JSONObject()
                .put("amount",10000);
    }

    public JSONObject jsonObjectPaymentPageAuthorizationNoAmountRequestBody(){
        return new JSONObject()
                .put("orderId","authorizarion-order")
                .put("currency","EUR")
                .put("returnUrl","https://your_domain.com/return")
                .put("responseUrl","https://your_domain.com/confirm")
                .put("usePaymentMeanToken",true)
                .put("customerId","user-1234");
    }

    public JSONObject jsonObjectPaymentPageAuthorizationEmptyRequestBody(){
        return new JSONObject();
    }
}