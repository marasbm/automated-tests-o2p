package accountamount;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class AccountAmountRequest {

    private final ConfigurationsProperties configurationsProperties;

    public AccountAmountRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getAccountAmount(String authorizationKey, String authorizationValue, String beneficiary) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.accountAmountBaseUri().concat(beneficiary));
    }
}
