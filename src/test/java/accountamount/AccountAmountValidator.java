package accountamount;

import configurations.ConfigurationsProperties;
import org.json.JSONObject;


public class AccountAmountValidator {

    private static ConfigurationsProperties configurationsProperties;

    public AccountAmountValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public String getAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }
}
