package balances;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class BalancesRequest {

    private final ConfigurationsProperties configurationsProperties;

    public BalancesRequest(){

        configurationsProperties = new ConfigurationsProperties();
    }

    public Response postBalancesRequest(String authorizationKey, String authorizationValue, String requestBody) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment().concat(
                configurationsProperties.balancesBaseUri());

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post();
    }

    public Response getBalancesRequest(String authorizationKey, String authorizationValue, String queryString) {

        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }

    public Response getBalancesByIdRequest(String authorizationKey, String authorizationValue, String queryString) {

        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }

    public String balancesCompleteRequiredFieldsRequestBody(String beneficiary1, String beneficiary2){
        return "[\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\""+beneficiary1+"\",\n" +
                "\"sender\":\""+beneficiary2+"\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balancesCompleteRequiredFieldsAndTransactionIdRequestBody(String beneficiary, String transactionId){
        return "[\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\""+beneficiary+"\",\n" +
                "\"sender\":\""+beneficiary+"\",\n" +
                "\"echoedMessage\":\"TEST\",\n" +
                "\"transactionId\":\""+transactionId+"\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNoRequiredFieldsRequestBody(){
        return "[\n" +
                "{\n" +
                "\"subOrderId\":\"\",\n" +
                "\"echoedMessage\":\"TEST\",\n" +
                "\"transactionId\":\"5c89276ad3ea2730d4bf72ee\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNoRequiredFieldAmountRequestBody(){
        return "[\n" +
                "{\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\"654321\",\n" +
                "\"sender\":\"654321\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNullRequiredFieldAmountRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"null\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\"654321\",\n" +
                "\"sender\":\"654321\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNonValidRequiredFieldAmountRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"100.1\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\"654321\",\n" +
                "\"sender\":\"654321\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNoRequiredFieldSenderRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\"654321\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balancesWithNonValidRequiredFieldReceiverRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"\",\n" +
                "\"receiver\":\"000000\",\n" +
                "\"sender\":\"654321\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithAnInvalidTransactionIdRequestBody(){
        return "[\n" +
                "{\n" +
                "\"transactionId\":\"INVALID_TRANSACTION_ID\",\n" +
                "\"amount\":\"200\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1552322261903000\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1552322261903000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST2\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithNotUniqueIdRequestBody(String balanceId){
        return "[\n" +
                "{\n" +
                "\"balanceId\":\""+balanceId+"\",\n" +
                "\"amount\":\"200\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1552322261903000\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"100\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1552322261903000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST2\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithTheSameSenderAndReceiverRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"500\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"600\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST2\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithoutIdsValidValuesAndBeneficiariesRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"500\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1552322261903000\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"600\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1552322261903000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST2\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithoutIdsAndInvalidAmountValueOnOneRecordRequestBody(){
        return "[\n" +
                "{\n" +
                "\"amount\":\"500\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1551952366864000\",\n" +
                "\"sender\":\"1552322261903000\",\n" +
                "\"echoedMessage\":\"TEST\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"600000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"receiver\":\"1552322261903000\",\n" +
                "\"sender\":\"1551952366864000\",\n" +
                "\"echoedMessage\":\"TEST2\"\n" +
                "}\n" +
                "]";
    }

    public String balanceWithCreatedIdAndValidValuesRequestBody(String balanceId){
        return "[\n" +
                "    {\n" +
                "    \t\"balanceId\": \""+balanceId+"\",\n" +
                "        \"amount\": \"100\",\n" +
                "        \"subOrderId\": \"123\",\n" +
                "        \"receiver\": \"1551952366864000\",\n" +
                "        \"sender\": \"1552322261903000\",\n" +
                "        \"echoedMessage\": \"TEST\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"amount\": \"100\",\n" +
                "        \"subOrderId\": \"123\",\n" +
                "        \"receiver\": \"1552322261903000\",\n" +
                "        \"sender\": \"1551952366864000\",\n" +
                "        \"echoedMessage\": \"TEST2\"\n" +
                "    }\n" +
                "]";
    }

    public String balanceWithGeneratedIdAndValidValuesRequestBody(){
        return "[\n" +
                "    {\n" +
                "        \"amount\": \"100\",\n" +
                "        \"subOrderId\": \"123\",\n" +
                "        \"receiver\": \"1552322261903000\",\n" +
                "        \"sender\": \"1551952366864000\",\n" +
                "        \"echoedMessage\": \"TEST2\"\n" +
                "    }\n" +
                "]";
    }
}