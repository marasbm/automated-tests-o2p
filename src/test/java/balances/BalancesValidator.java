package balances;

import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class BalancesValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BalancesValidator() {

        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getBalancesToJSonObject(JSONArray jsonArray) {
        JSONObject balancesToJsonObject = new JSONObject();

        for (int i = 0; i < jsonArray.length(); i++) {
            balancesToJsonObject = jsonArray.getJSONObject(i);
        }

        return balancesToJsonObject;
    }

    public String getAmount(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.amount()).toString();
    }

    public String getReceiver(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.receiver()).toString();
    }

    public String getSender(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.sender()).toString();
    }

    public String getCreationDate(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.creationDate()).toString();
    }

    public String getEchoedMessage(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.echoedMessage()).toString();
    }

    public String getStatus(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.status()).toString();
    }

    public String getBalanceId(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.balanceId()).toString();
    }

    public String getTransactionsId(JSONArray jsonArray) {
        return getBalancesToJSonObject(jsonArray).get(configurationsProperties.transactionId()).toString();
    }

    private JSONArray getBalancesItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getBalancesItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getBalancesItems(jsonObj).length(); i++) {
            JSONObject object = getBalancesItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public List<String> getBalancesIds(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.balanceId()
        ).toString()).collect(Collectors.toList());
    }

    public List<String> getBalancesAmount(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.amount()
        ).toString()).collect(Collectors.toList());
    }

    public List<String> getBalancesReceiver(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.receiver()
        ).toString()).collect(Collectors.toList());
    }

    public List<String> getBalancesSender(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.sender()
        ).toString()).collect(Collectors.toList());
    }

    public List<String> getBalancesCreationDate(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.creationDate()
        ).toString()).collect(Collectors.toList());
    }

    public List<String> getBalancesStatus(JSONObject jsonObj){
        return getBalancesItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.status()
        ).toString()).collect(Collectors.toList());
    }

    public String getBalancesById(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getBalancesByIdReceiver(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.receiver()).toString();
    }

    public String getBalancesByIdSender(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.sender()).toString();
    }

    public String getBalancesByIdCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getBalancesByIdEchoedMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.echoedMessage()).toString();
    }

    public String getBalancesByIdStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getBalancesByIdBalanceId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.balanceId()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    private List<JSONObject> balancesWithoutItems(JSONArray jsonArray){
        List<JSONObject> balancesToListJsonObject = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            balancesToListJsonObject.add(jsonArray.getJSONObject(i));
        }

        return balancesToListJsonObject;
    }

    private List<String> balancesStatusWithoutItems(JSONArray jsonArray){
       return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.status()).toString())
               .collect(Collectors.toList());
    }

    private List<String> balancesAmountWithoutItems(JSONArray jsonArray){
        return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.amount()).toString())
                .collect(Collectors.toList());
    }

    private List<String> balancesCreationDateWithoutItems(JSONArray jsonArray){
        return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.creationDate()).toString())
                .collect(Collectors.toList());
    }

    private List<String> balancesIdsWithoutItems(JSONArray jsonArray){
        return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.balanceId()).toString())
                .collect(Collectors.toList());
    }

    private List<String> balancesSenderWithoutItems(JSONArray jsonArray){
        return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.sender()).toString())
                .collect(Collectors.toList());
    }

    private List<String> balancesReceiverWithoutItems(JSONArray jsonArray){
        return balancesWithoutItems(jsonArray).stream().map(x->x.get(configurationsProperties.receiver()).toString())
                .collect(Collectors.toList());
    }

    public boolean validateAllBalancesInRejectedStatus(JSONArray jsonArray){
        return balancesStatusWithoutItems(jsonArray).stream().allMatch(x->x.equals("REJECTED"));
    }

    public boolean validateAllBalancesInDoneOrRejectedStatus(JSONArray jsonArray){
        return balancesStatusWithoutItems(jsonArray).stream().allMatch(x->x.equals("DONE") || x.equals("REJECTED"));
    }

    public boolean validateAllBalancesAmountGraterThanZero(JSONArray jsonArray){
        return balancesAmountWithoutItems(jsonArray).stream().allMatch(x-> Integer.parseInt(x) > 0);
    }

    public boolean validateAllBalancesCreationDate(JSONArray jsonArray){
        return balancesCreationDateWithoutItems(jsonArray).stream().allMatch(
                HelpersValidator::validateDateFormatWithMills);
    }

    public boolean validateAllBalanceIdsNotEquals(JSONArray jsonArray){
        return balancesIdsWithoutItems(jsonArray).size() == balancesIdsWithoutItems(jsonArray).stream().distinct().count();
    }

    public boolean validateSameSenderAndReceiver(JSONArray jsonArray){
        AtomicBoolean asTheSame = new AtomicBoolean(false);

        balancesSenderWithoutItems(jsonArray).forEach(
                item-> {
                    for (int i = 0; i< balancesReceiverWithoutItems(jsonArray).size(); i++){
                        if(item.equals(balancesReceiverWithoutItems(jsonArray).get(i))){
                            asTheSame.set(true);
                        }

                    }
                });

       return asTheSame.get();
    }

}