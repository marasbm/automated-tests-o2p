package beneficiaries;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class BeneficiariesRequest {

    private ConfigurationsProperties configurationsProperties;

    public BeneficiariesRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getBeneficiariesRequest(String authorizationKey, String authorizationValue, String queryString) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }

    public Response putUpdateBeneficiaryStatus(String authKey, String authValue, String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.beneficiariesUsersBaseUri().concat("/").concat(
                configurationsProperties.beneficiaryToUpdate()));
    }

    public Response putUpdateBeneficiaryStatusInvalidEndPoint(String queryString, String endPoint) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(endPoint);
    }

    public String updateBeneficiaryStatusRequestBody(String status, String kycStatus){
        return "{\n" +
                "\"status\":\""+status+"\",\n" +
                "\"KYC_status\":\""+kycStatus+"\"\n" +
                "}";
    }

    public String updateBeneficiaryOnlyFieldLegalName(String legalname){
        return "{\n" +
                "\"legalName\":\""+legalname+"\"\n" +
                "}";
    }

    public Response getBeneficiaries(String authKey, String authValue) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.beneficiariesUsersBaseUri());
    }

    public Response getBeneficiaryById(String authKey, String authValue, String beneficiaryId) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.beneficiariesUsersBaseUri().concat("/").concat(beneficiaryId));
    }

    public Response putUpdateBeneficiary(String authKey, String authValue, String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.beneficiariesUsersBaseUri().concat("/").concat(
                configurationsProperties.beneficiaryToUpdate()));
    }

    public Response putUpdateBeneficiaryWithoutBeneficiary(String authKey, String authValue, String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.beneficiariesUsersBaseUri());
    }

    public Response putUpdateBeneficiaryInvalidBeneficiary(String authKey, String authValue, String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.beneficiariesUsersBaseUri().concat(
                configurationsProperties.invalidBeneficiary()));
    }

    public Response putUpdateBeneficiaryToMarketPlace(String queryString) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.marketplacesBaseUri().concat(
                configurationsProperties.beneficiaryToUpdate()));
    }

    public String updateBeneficiaryRequestBody(String legalName, String email){
        return "{\n" +
                "\"status\":\"ACTIVATED\",\n" +
                "\"legalName\":\""+legalName+"\",\n" +
                "\"group\":\"string_updated\",\n" +
                "\"address\":{\n" +
                "\"address1\":\"string_updated\",\n" +
                "\"address2\":\"string_updated\",\n" +
                "\"address3\":\"string_updated\",\n" +
                "\"address4\":\"string_updated\",\n" +
                "\"zipCode\":\"string_updated\",\n" +
                "\"country\":\"FR\",\n" +
                "\"city\":\"string_updated\"\n" +
                "},\n" +
                "\"registrationId\":\"string_updated\",\n" +
                "\"internalId\":\"string_updated\",\n" +
                "\"email\":\""+email+"\",\n" +
                "\"category\":\"string_updated\",\n" +
                "\"KYC_status\":\"KYC_VALIDATED\"\n" +
                "}";
    }

    public Response putUpdateBeneficiaryInvalidEndPoint(String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(queryString);

        return httpRequest.put(configurationsProperties.beneficiariesUsersBaseUri().concat(
                configurationsProperties.beneficiaryToUpdate()).concat("status"));
    }
}