package beneficiaries;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


public class BeneficiariesValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BeneficiariesValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public String getDescription(JSONArray jsonArray) {
        return getItemArrayToJSonObject(jsonArray).get(configurationsProperties.description()).toString();
    }

    private JSONObject getItemArrayToJSonObject(JSONArray jsonArray) {
        return jsonArray.getJSONObject(0);
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    public String getStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getBeneficiaryId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.beneficiaryId()).toString();
    }

    public String getEmail(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.email()).toString();
    }

    public String getLegalName(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.legalName()).toString();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public String getTotalElements(JSONObject jsonObj) {
        return getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString();
    }

    private JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getItems(jsonObj).length(); i++) {
            JSONObject object = getItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    private String getBeneficiaryByIdStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    private String getBeneficiaryByIdKycStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.kycStatus()).toString();
    }

    public boolean validateBeneficiaryByIdStatus(JSONObject jsonObj) {
        return getBeneficiaryByIdStatus(jsonObj).equals(configurationsProperties.notActivated())||
                getBeneficiaryByIdStatus(jsonObj).equals(configurationsProperties.activated()) ||
                getBeneficiaryByIdStatus(jsonObj).equals(configurationsProperties.deleted());
    }

    public boolean validateBeneficiaryByIdkycStatus(JSONObject jsonObj) {
        return getBeneficiaryByIdKycStatus(jsonObj).equals(configurationsProperties.kycNotLoaded())||
                getBeneficiaryByIdKycStatus(jsonObj).equals(configurationsProperties.kycLoaded()) ||
                getBeneficiaryByIdKycStatus(jsonObj).equals(configurationsProperties.kycValidated()) ||
                getBeneficiaryByIdKycStatus(jsonObj).equals(configurationsProperties.kycRejected());
    }

    public String getBeneficiaryByIdKycStatusValue(JSONObject jsonObject){
        return getBeneficiaryByIdKycStatus(jsonObject);
    }

    public boolean validateAllStatus(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(
                x-> x.get(configurationsProperties.status()).toString()
                        .equals(configurationsProperties.notActivated())||
                        x.get(configurationsProperties.status()).toString()
                                .equals(configurationsProperties.activated()) ||
                        x.get(configurationsProperties.status()).toString()
                                .equals(configurationsProperties.deleted()));
    }

    public boolean validateAllkycStatus(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(
                x-> x.get(configurationsProperties.kycStatus()).toString()
                        .equals(configurationsProperties.kycNotLoaded())||
                        x.get(configurationsProperties.kycStatus()).toString()
                                .equals(configurationsProperties.kycLoaded()) ||
                        x.get(configurationsProperties.kycStatus()).toString()
                                .equals(configurationsProperties.kycValidated()) ||
                        x.get(configurationsProperties.kycStatus()).toString()
                                .equals(configurationsProperties.kycRejected()));
    }

    public String getItemsBeneficiaryId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("No items found")).get(configurationsProperties.beneficiaryId()).toString();
    }

    public String getItemsLegalName(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("No items found")).get(configurationsProperties.legalName()).toString();
    }

    private List<String> getAllItemsLegalName(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.legalName()).toString())
                .collect(Collectors.toList());
    }

    public boolean validatePartialLegalnameInList(JSONObject jsonObj, String partialLegalName){
        return getAllItemsLegalName(jsonObj).stream().allMatch(x->x.contains(partialLegalName));
    }

    public String getItemsEmail(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("No items found")).get(configurationsProperties.email()).toString();
    }

    private List<String> getAllItemsEmail(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.email()).toString())
                .collect(Collectors.toList());
    }

    public boolean validatePartialEmailInList(JSONObject jsonObj, String partialEmail){
        return getAllItemsEmail(jsonObj).stream().allMatch(x->x.contains(partialEmail));
    }
}
