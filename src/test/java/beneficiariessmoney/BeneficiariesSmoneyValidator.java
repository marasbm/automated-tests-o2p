package beneficiariessmoney;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class BeneficiariesSmoneyValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BeneficiariesSmoneyValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public JSONObject getAddress(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.address());
    }

    public String getKycStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.kycStatus()).toString();
    }

    public String getLegalName(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.legalName()).toString();
    }

    public String getPayoutRulesDefined(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.payoutRulesDefined()).toString();
    }

    public String getEmail(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.email()).toString();
    }

    public String getStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getBeneficiaryId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.beneficiaryId()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    private List<JSONObject> getSmoneyBeneficiariesToJSonObject(JSONArray jsonArray) {
        List<JSONObject> list = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            list.add(object);
        }

        return list;
    }

    public List<JSONObject> getSmoneyBeneficiariesProfileToJSonObject(JSONArray jsonArray) {
        List<JSONObject> list = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = getSmoneyBeneficiariesToJSonObject(jsonArray).get(i).getJSONObject(
                    configurationsProperties.smoneyBeneficiaryProfile());
            list.add(object);
        }

        return list;
    }
}

