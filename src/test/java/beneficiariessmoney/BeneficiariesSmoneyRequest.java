package beneficiariessmoney;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class BeneficiariesSmoneyRequest {

    private ConfigurationsProperties configurationsProperties;

    public BeneficiariesSmoneyRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response createBeneficiarySmoney(String authKey, String authValue, String requestBody) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.beneficiariesUsersBaseUri());
    }

    public Response getBeneficiariesSmoney() {
        RequestSpecification httpRequest = given();

        httpRequest.header(configurationsProperties.authorization(),
                "Bearer ".concat(configurationsProperties.smoneyToken()));

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.smoneyUsersBaseUri());
    }

    public Response getCreatedBeneficiarySmoney(String email) {
        RequestSpecification httpRequest = given();

        httpRequest.header(configurationsProperties.authorization(),
                "Bearer ".concat(configurationsProperties.smoneyToken()));

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.smoneyUsersBaseUri().concat("?email=").concat(email));
    }

    public String createBeneficiarySmoneyRequestBody(String email){
        return "{\n" +
                "\"email\":\""+email+"\",\n" +
                "\"legalName\":\"BENEF. DEMO 25\",\n" +
                "\"address\":{\n" +
                "\"address1\":\"address1\",\n" +
                "\"address2\":\"address2\",\n" +
                "\"address3\":\"address3\",\n" +
                "\"address4\":\"address4\",\n" +
                "\"city\":\"PORTO\",\n" +
                "\"zipCode\":\"4400320\",\n" +
                "\"country\":\"PT\"\n" +
                "}\n" +
                "}";
    }

    public String createBeneficiarySmoneyRequestBodyNotValidEmail(String email){
        return "{\n" +
                "\"email\":\""+email+"\",\n" +
                "\"legalName\":\"BENEF. DEMO 25\",\n" +
                "\"address\":{\n" +
                "\"address1\":\"address1\",\n" +
                "\"address2\":\"address2\",\n" +
                "\"address3\":\"address3\",\n" +
                "\"address4\":\"address4\",\n" +
                "\"city\":\"PORTO\",\n" +
                "\"zipCode\":\"4400320\",\n" +
                "\"country\":\"PT\"\n" +
                "}\n" +
                "}";
    }

    public String createBeneficiarySmoneyRequestBodyEmptyEmail(){
        return "{\n" +
                "\"email\":\"\",\n" +
                "\"legalName\":\"BENEF. DEMO 25\",\n" +
                "\"address\":{\n" +
                "\"address1\":\"address1\",\n" +
                "\"address2\":\"address2\",\n" +
                "\"address3\":\"address3\",\n" +
                "\"address4\":\"address4\",\n" +
                "\"city\":\"PORTO\",\n" +
                "\"zipCode\":\"4400320\",\n" +
                "\"country\":\"PT\"\n" +
                "}\n" +
                "}";
    }
}