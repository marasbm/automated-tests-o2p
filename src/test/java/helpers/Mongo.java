package helpers;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import configurations.ConfigurationsProperties;

import java.net.UnknownHostException;

public class Mongo {

    public static void deleteCreatedUser(String email) throws UnknownHostException {
        ConfigurationsProperties configurationsProperties = new ConfigurationsProperties();

        MongoClient mongoClient = new MongoClient( configurationsProperties.mongoServer(),
                Integer.parseInt(configurationsProperties.mongoServerPort()));

        DB database = mongoClient.getDB("mp_users");

        DBCollection collection = database.getCollection("users");

        BasicDBObject document = new BasicDBObject();

        document.put("email", email);

        collection.remove(document);
    }
}