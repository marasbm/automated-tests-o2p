package helpers;

import com.jayway.restassured.response.Response;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HelpersValidator {

    public static JSONObject getResponseBodyToJsonObject(Response response) {
        return new JSONObject(response.body().prettyPrint());
    }

    public static JSONArray getResponseBodyToJsonArray(Response response){
        return new JSONArray(response.body().prettyPrint());
    }

    public static boolean validateDateFormatNoMills(String inputDate){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
                .withZone(ZoneId.of("UTC"));

        try {
            LocalDate.parse(inputDate, dateTimeFormatter);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean validateDateFormatWithMills(String inputDate){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
                .withZone(ZoneId.of("UTC"));

        try {
            LocalDate.parse(inputDate, dateTimeFormatter);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean validateCronExpression(String cronExpression){
      return  org.quartz.CronExpression.isValidExpression(cronExpression);
    }

    public static String generateSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

    public static void sleepFourSeconds() throws InterruptedException {
        Thread.sleep(4000);
    }
}


