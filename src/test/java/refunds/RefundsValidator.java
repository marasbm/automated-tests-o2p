package refunds;

import configurations.ConfigurationsProperties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RefundsValidator {

    private static ConfigurationsProperties configurationsProperties;

    public RefundsValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public String getTotalElements(JSONObject jsonObj) {
        return getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString();
    }

    public JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getItems(jsonObj).length(); i++) {
            JSONObject object = getItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public boolean getAmount(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.amount()).toString()));
    }

    public boolean getAmountGreaterThan100(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> Integer.parseInt(
                String.valueOf(x.get(configurationsProperties.amount()))) > 100);
    }

    public boolean getPaymentId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.paymentId()).toString()));
    }

    public boolean getCreationDate(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.creationDate()).toString()));
    }

    public List<String> getCreationDateList(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.creationDate()
        ).toString()).collect(Collectors.toList());
    }

    public boolean getStatus(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.status()).toString()));
    }

    public String getRefundAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getRefundPaymentId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.paymentId()).toString();
    }

    public String getRefundCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getRefundStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }
}
