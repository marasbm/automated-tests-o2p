package tests.paymentpageauthorization;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import paymentpageauthorization.PaymentPageAuthorizationRequest;
import paymentpagevalidator.PaymentPageValidator;
import token.GeneratedTokenProperties;

public class ValidPaymentPageAuthorization {

    private static PaymentPageAuthorizationRequest paymentPageAuthorizationRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private PaymentPageValidator paymentPageAuthorizationValidator;

    @BeforeClass
    public static void initClass(){
        paymentPageAuthorizationRequest = new PaymentPageAuthorizationRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        paymentPageAuthorizationValidator = new PaymentPageValidator();
    }

    @Test
    public void securityPaymentPageUrl(){
        Response response = paymentPageAuthorizationRequest.paymentPageAuthorization(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageAuthorizationRequest.jsonObjectPaymentPageAuthorizationCompleteRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        Assert.assertTrue("It was expected the amount value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getAmount(jsonObject)));

        Assert.assertTrue("It was expected the orderId value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getOrderId(jsonObject)));

        Assert.assertTrue("It was expected the transactionId value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getTransactionId(jsonObject)));

        Assert.assertTrue("It was expected the responseUrl value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getResponseUrl(jsonObject)));

        Assert.assertTrue("It was expected the customerId value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getCustomerId(jsonObject)));

        Assert.assertEquals("It was expected currency EUR", "EUR",
                paymentPageAuthorizationValidator.getCurrency(jsonObject));

        Assert.assertTrue("It was expected the web page url with token value",
                paymentPageAuthorizationValidator.getPaymentPageUrl(jsonObject).contains("token="));

        Assert.assertTrue("It was expected the returnUrlValue value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getReturnUrl(jsonObject)));

        Assert.assertFalse(paymentPageAuthorizationValidator.getPaymentPageUrl(jsonObject).contains("transactionId"));
    }

    @Test
    public void securityPaymentPageUrlWithOnlyAmountInRequestBody(){
        Response response = paymentPageAuthorizationRequest.paymentPageAuthorization(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageAuthorizationRequest.jsonObjectPaymentPageAuthorizationOnlyAmountRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        Assert.assertTrue("It was expected the amount value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getAmount(jsonObject)));

        Assert.assertTrue("It was expected the transactionId value",
                StringUtils.isNotEmpty(paymentPageAuthorizationValidator.getTransactionId(jsonObject)));


        Assert.assertTrue("It was expected the web page url with token value",
                paymentPageAuthorizationValidator.getPaymentPageUrl(jsonObject).contains("token="));

        Assert.assertFalse(paymentPageAuthorizationValidator.getPaymentPageUrl(jsonObject).contains("transactionId"));
    }
}