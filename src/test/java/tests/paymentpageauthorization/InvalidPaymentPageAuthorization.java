package tests.paymentpageauthorization;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import paymentpageauthorization.PaymentPageAuthorizationRequest;
import paymentpagevalidator.PaymentPageValidator;
import token.GeneratedTokenProperties;

public class InvalidPaymentPageAuthorization {

    private static PaymentPageAuthorizationRequest paymentPageAuthorizationRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private PaymentPageValidator paymentPageAuthorizationValidator;

    @BeforeClass
    public static void init(){
        paymentPageAuthorizationRequest = new PaymentPageAuthorizationRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        paymentPageAuthorizationValidator = new PaymentPageValidator();
    }

    @Test
    public void invalidRequestPaymentPageUrlNoRequiredFieldAmount(){
        Response response = paymentPageAuthorizationRequest.paymentPageAuthorization(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageAuthorizationRequest.jsonObjectPaymentPageAuthorizationNoAmountRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected missing: amount",
                "The following parameters are missing: amount",
                paymentPageAuthorizationValidator.getDescription(jsonObject));

        Assert.assertEquals("It was expected MISSING_PARAM", "MISSING_PARAM",
                paymentPageAuthorizationValidator.getType(jsonObject));
    }

    @Test
    public void invalidRequestPaymentPageUrlBodyEmpty(){
        Response response = paymentPageAuthorizationRequest.paymentPageAuthorization(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageAuthorizationRequest.jsonObjectPaymentPageAuthorizationEmptyRequestBody());

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected missing: amount",
                "The following parameters are missing: amount",
                paymentPageAuthorizationValidator.getDescription(jsonObject));

        Assert.assertEquals("It was expected MISSING_PARAM", "MISSING_PARAM",
                paymentPageAuthorizationValidator.getType(jsonObject));
    }
}
