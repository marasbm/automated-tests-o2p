package tests.authorizations;

import authorizations.AuthorizationsRequest;
import authorizations.AuthorizationsValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;

public class InvalidAuthorizations {

    private static AuthorizationsRequest authorizationsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private AuthorizationsValidator authorizationsValidator;

    @BeforeClass
    public static void initClass(){
        authorizationsRequest = new AuthorizationsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        authorizationsValidator = new AuthorizationsValidator();
    }

    @Test
    public void getInvalidAuthorizations(){
        Response response = authorizationsRequest.getAuthorizations(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidTransactionsAuthorization());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        Assert.assertEquals("It was expected the error Not Found",
                "Not Found",authorizationsValidator.getError(jsonObject));

    }
}