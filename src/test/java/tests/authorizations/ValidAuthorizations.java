package tests.authorizations;

import authorizations.AuthorizationsRequest;
import authorizations.AuthorizationsValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;

import java.util.NoSuchElementException;


public class ValidAuthorizations {

    private static AuthorizationsRequest authorizationsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private AuthorizationsValidator authorizationsValidator;

    @BeforeClass
    public static void initClass(){
        authorizationsRequest = new AuthorizationsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        authorizationsValidator = new AuthorizationsValidator();
    }

    @Test
    public void getAuthorizations(){
        Response response = authorizationsRequest.getAuthorizations(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsAuthorizations());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        Assert.assertTrue("It was expected the totalElements > 0", Integer.parseInt(
                authorizationsValidator.getTotalElements(jsonObject)) > 0);

        Assert.assertNotNull("It was expected the items value",authorizationsValidator.getItems(jsonObject));

        Assert.assertEquals("It was expected the same value", Integer.parseInt(
                authorizationsValidator.getTotalElements(jsonObject)),
                authorizationsValidator.getItems(jsonObject).length());

        Assert.assertTrue("It was expected the amount value",authorizationsValidator.getAmount(jsonObject));

        Assert.assertTrue("It was expected the authorizationId value",
                authorizationsValidator.getAuthorizationId(jsonObject));

        Assert.assertTrue("It was expected the creationDate value",
                authorizationsValidator.getCreationDate(jsonObject));

        Assert.assertTrue("It was expected the status value",authorizationsValidator.getStatus(jsonObject));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatNoMills(authorizationsValidator.getCreationDateList(jsonObject)
                        .stream().findFirst().orElseThrow(()-> new NoSuchElementException("creation date are empty"))));

        Assert.assertTrue("It was expected the transactionId value",
                authorizationsValidator.getTransactionId(jsonObject));
    }

    @Test
    public void getAuthorizationsById(){
        Response response = authorizationsRequest.getAuthorizations(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsAuthorization());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected a value greater than 100",
                Integer.parseInt(authorizationsValidator.getAuthorizationAmount(jsonObject)) > 100);

        Assert.assertTrue("It was expected a getCapturePaymentId value",
                StringUtils.isNotEmpty(authorizationsValidator.getAuthorizationAuthorizationId(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatNoMills(
                        authorizationsValidator.getAuthorizationCreationDate(jsonObject)));

        Assert.assertTrue("It was expected a getCaptureStatus value",
                StringUtils.isNotEmpty(authorizationsValidator.getAuthorizationStatus(jsonObject)));

        Assert.assertTrue("It was expected a getAuthorizationTransactionId value",
                StringUtils.isNotEmpty(authorizationsValidator.getAuthorizationTransactionId(jsonObject)));

        //TODO: Bug found FN4OC – 815

    }
}