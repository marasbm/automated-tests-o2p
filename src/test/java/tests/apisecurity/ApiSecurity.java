package tests.apisecurity;

import apisecurity.ApiSecurityRequest;
import apisecurity.ApiSecurityValidator;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApiSecurity {

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private ApiSecurityValidator apiSecurityValidator;

    @BeforeClass
    public static void initClass(){
        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        apiSecurityValidator = new ApiSecurityValidator(configurationsProperties);
    }

    @Test
    public void notAuthorizedClientNoHttpHeaderAuthorizationTransactionsTransactionsEndPoint() {
        Response response = ApiSecurityRequest.getNotAuthorizedClientNoHttpHeaderAuthorization(
                configurationsProperties.integrationEnvironment());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void notAuthorizedClientNonValidHeaderAuthorizationTransactionsEndPoint() {
        Response response = ApiSecurityRequest.getMasterPayApiAuthorizationClient(
                configurationsProperties.integrationEnvironment(), configurationsProperties.authorization(),
                configurationsProperties.invalidAuthorization(),ContentType.URLENC);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void authorizeClientValidHeaderAuthorizationTransactionsEndPoint() {
        Response response = ApiSecurityRequest.getMasterPayApiAuthorizationClient(
                configurationsProperties.integrationEnvironment(), configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(), ContentType.URLENC);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void authorizeClientValidHeaderAuthorizationPayoutsEndPoint() {
        Response response = ApiSecurityRequest.getMasterPayApiAuthorizationClient(
                configurationsProperties.integrationEnvironment(),configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(), ContentType.URLENC);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void NotAuthorizedClientNoHttpHeaderAuthorizationPayoutsEndPoint() {
        Response response = ApiSecurityRequest.getNotAuthorizedClientNoHttpHeaderAuthorization(
                configurationsProperties.integrationEnvironment());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void NotAuthorizedClientNonValidHeaderAuthorizationTransferEndPoint() {
        Response response = ApiSecurityRequest.getMasterPayApiAuthorizationClient(
                configurationsProperties.integrationEnvironment(), configurationsProperties.authorization(),
                configurationsProperties.invalidAuthorization(), ContentType.URLENC);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void NotAuthorizedClientNoHttpHeaderAuthorizationPaymentEndPoint() {
        Response response = ApiSecurityRequest.getNotAuthorizedClientNoHttpHeaderAuthorization(
                configurationsProperties.integrationEnvironment());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }

    @Test
    public void InvalidTokenPayoutsEndPoint() {
        Response response = ApiSecurityRequest.getMasterPayApiAuthorizationClient(
                configurationsProperties.integrationEnvironment(),configurationsProperties.authorization(),
                configurationsProperties.inValidToken(), ContentType.URLENC);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("Not Found", apiSecurityValidator.getError(jsonObject));
    }
}