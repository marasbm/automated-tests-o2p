package tests.beneficiariessmoney;

import beneficiaries.BeneficiariesRequest;
import beneficiaries.BeneficiariesValidator;
import beneficiariessmoney.BeneficiariesSmoneyRequest;
import beneficiariessmoney.BeneficiariesSmoneyValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidBeneficiariesSmoney {

    private static BeneficiariesSmoneyRequest beneficiariesSmoneyRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesSmoneyValidator beneficiariesSmoneyValidator;

    private String email;

    @BeforeClass
    public static void initClass(){
        beneficiariesSmoneyRequest = new BeneficiariesSmoneyRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesSmoneyValidator = new BeneficiariesSmoneyValidator();

        email = HelpersValidator.generateSaltString().concat("@email.com");
    }

    @Test
    public void createBeneficiarySmoney(){
        Response response = beneficiariesSmoneyRequest.createBeneficiarySmoney(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesSmoneyRequest.createBeneficiarySmoneyRequestBody(email));

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected an address object",
                beneficiariesSmoneyValidator.getAddress(jObject).length() > 0);

        Assert.assertEquals("It was expected the same kyc_status",
                beneficiariesSmoneyValidator.getKycStatus(jObject), configurationsProperties.kycNotLoaded());

        Assert.assertTrue("It was expected a not empty legalname",
                StringUtils.isNotEmpty(beneficiariesSmoneyValidator.getLegalName(jObject)));

        Assert.assertTrue("It was expected true",
                Boolean.parseBoolean(beneficiariesSmoneyValidator.getPayoutRulesDefined(jObject)));

        Assert.assertEquals("It was expected the same email",
                beneficiariesSmoneyValidator.getEmail(jObject), email);

        Assert.assertEquals("It was expected the same status", beneficiariesSmoneyValidator.getStatus(jObject),
                configurationsProperties.notActivated());

        Assert.assertTrue("It was expected a not beneficiaryId",
                StringUtils.isNotEmpty(beneficiariesSmoneyValidator.getBeneficiaryId(jObject)));

        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getBeneficiariesSmoney();

        Assert.assertEquals("It was expected the status code 200",200,
                responseGetBeneficiarySmoney.statusCode());

        Response responseGetBeneficiaryCreatedSmoney = beneficiariesSmoneyRequest.getCreatedBeneficiarySmoney(email);

        Assert.assertEquals("It was expected the status code 200",200,
                responseGetBeneficiaryCreatedSmoney.statusCode());

        JSONArray jObjectGetBeneficiaryCreatedSmoney = HelpersValidator.getResponseBodyToJsonArray(
                responseGetBeneficiaryCreatedSmoney);

        Assert.assertEquals("It was expected one beneficiary",
                1, jObjectGetBeneficiaryCreatedSmoney.length());

    }

    @Test
    public void getBeneficiarySmoney(){
        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getBeneficiariesSmoney();

        Assert.assertEquals("It was expected the status code 200",200,
                responseGetBeneficiarySmoney.statusCode());

        JSONArray jObjectGetBeneficiarySmoney = HelpersValidator.getResponseBodyToJsonArray(
                responseGetBeneficiarySmoney);

        List<JSONObject> getSmoneyBeneficiariesProfile =
                beneficiariesSmoneyValidator.getSmoneyBeneficiariesProfileToJSonObject(jObjectGetBeneficiarySmoney);

        Assert.assertTrue("It was expected one beneficiary", getSmoneyBeneficiariesProfile.size() > 0);

    }
}