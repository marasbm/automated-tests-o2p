package tests.beneficiariessmoney;

import beneficiariessmoney.BeneficiariesSmoneyRequest;
import beneficiariessmoney.BeneficiariesSmoneyValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvalidBeneficiariesSmoney {

    private static BeneficiariesSmoneyRequest beneficiariesSmoneyRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesSmoneyValidator beneficiariesSmoneyValidator;

    private String email;

    @BeforeClass
    public static void initClass(){
        beneficiariesSmoneyRequest = new BeneficiariesSmoneyRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesSmoneyValidator = new BeneficiariesSmoneyValidator();

        email = HelpersValidator.generateSaltString().concat("@email.com");
    }

    @Test
    public void invalidCreateBeneficiarySmoneyEmptyRequestBody(){
        Response response = beneficiariesSmoneyRequest.createBeneficiarySmoney(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(), "");

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request", "Bad Request",
                beneficiariesSmoneyValidator.getError(jObject));

        Assert.assertTrue("It was expected Required request body is missing:",
                beneficiariesSmoneyValidator.getMessage(jObject).startsWith("Required request body is missing:"));

        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getCreatedBeneficiarySmoney(email);

        JSONArray jObjectGetBeneficiarySmoney = HelpersValidator.getResponseBodyToJsonArray(
                responseGetBeneficiarySmoney);

        Assert.assertEquals("It was expected one beneficiary",
                0, jObjectGetBeneficiarySmoney.length());
    }

    @Test
    public void invalidCreateBeneficiarySmoneyNotValidEmailRequestBody(){
        String notValidEmail = email.replace(".com","");

        Response response = beneficiariesSmoneyRequest.createBeneficiarySmoney(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesSmoneyRequest.createBeneficiarySmoneyRequestBodyNotValidEmail(notValidEmail));

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request", "Bad Request",
                beneficiariesSmoneyValidator.getError(jObject));

        Assert.assertEquals("It was expected Required request body is missing:",
                "Bad request to /createUser on SMoney Service", beneficiariesSmoneyValidator.getMessage(jObject));

        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getCreatedBeneficiarySmoney(notValidEmail);

        JSONArray jObjectGetBeneficiarySmoney = HelpersValidator.getResponseBodyToJsonArray(
                responseGetBeneficiarySmoney);

        Assert.assertEquals("It was expected one beneficiary",
                0, jObjectGetBeneficiarySmoney.length());
    }

    @Test
    public void invalidCreateBeneficiarySmoneyEmptyEmailRequestBody(){
        Response response = beneficiariesSmoneyRequest.createBeneficiarySmoney(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesSmoneyRequest.createBeneficiarySmoneyRequestBodyEmptyEmail());

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request", "Bad Request",
                beneficiariesSmoneyValidator.getError(jObject));

        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getCreatedBeneficiarySmoney("");

        JSONArray jObjectGetBeneficiarySmoney = HelpersValidator.getResponseBodyToJsonArray(
                responseGetBeneficiarySmoney);

        Assert.assertEquals("It was expected one beneficiary",
                0, jObjectGetBeneficiarySmoney.length());

        //TODO: Possible bug found created beneficiary with empty email
    }
}