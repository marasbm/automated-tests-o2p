package tests.balances;

import balances.BalancesRequest;
import balances.BalancesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;
import transactions.TransactionsRequest;
import transactions.TransactionsValidator;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidBalances {

    private static BalancesRequest balanceRequest;

    private static TransactionsRequest transactionsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BalancesValidator balanceValidator;

    private TransactionsValidator transactionsValidator;

    @BeforeClass
    public static void initClass(){
        balanceRequest = new BalancesRequest();

        transactionsRequest = new TransactionsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        balanceValidator = new BalancesValidator();

        transactionsValidator = new TransactionsValidator();
    }

    @Test
    public void createBalance(){
        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balancesCompleteRequiredFieldsRequestBody(
                        configurationsProperties.validBeneficiary(), configurationsProperties.otherValidBeneficiary()));

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue("It was expected the amount value",
                StringUtils.isNotEmpty(balanceValidator.getAmount(jsonArray)));

        Assert.assertTrue("It was expected the amount as integer value",
                Integer.parseInt(balanceValidator.getAmount(jsonArray)) > 0);

        Assert.assertTrue("It was expected the receiver value",
                StringUtils.isNotEmpty(balanceValidator.getReceiver(jsonArray)));

        Assert.assertTrue("It was expected the sender value",
                StringUtils.isNotEmpty(balanceValidator.getSender(jsonArray)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(balanceValidator.getCreationDate(jsonArray)));

        Assert.assertEquals("It was expected the echoedMessage TEST",
                balanceValidator.getEchoedMessage(jsonArray), "TEST");

        Assert.assertTrue("It was expected the status REJECTED or DONE",
                balanceValidator.getStatus(jsonArray).equals("REJECTED") ||
                        balanceValidator.getStatus(jsonArray).equals("DONE"));

        Response responseGetBalancesById = balanceRequest.getBalancesByIdRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/balances/" + balanceValidator.getBalanceId(jsonArray));

        JSONObject jsonObjectBalanceId = HelpersValidator.getResponseBodyToJsonObject(responseGetBalancesById);

        Assert.assertEquals("It was expected the same balanceId",
                balanceValidator.getBalanceId(jsonArray), jsonObjectBalanceId.get(configurationsProperties.balanceId()));
    }

    @Test
    public void createBalanceWithTransactionId(){
        Response responseGetTransactions = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList());

        Assert.assertEquals("It was expected the status code 206", 206,
                responseGetTransactions.statusCode());

        JSONObject getTransactions = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactions);

        String getTransactionId = transactionsValidator.getFirstTransactionId(getTransactions);

        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balancesCompleteRequiredFieldsAndTransactionIdRequestBody(
                        configurationsProperties.validBeneficiary(),getTransactionId));

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue("It was expected the amount value",
                StringUtils.isNotEmpty(balanceValidator.getAmount(jsonArray)));

        Assert.assertTrue("It was expected the amount as integer value",
                Integer.parseInt(balanceValidator.getAmount(jsonArray)) > 0);

        Assert.assertTrue("It was expected the receiver value",
                StringUtils.isNotEmpty(balanceValidator.getReceiver(jsonArray)));

        Assert.assertTrue("It was expected the sender value",
                StringUtils.isNotEmpty(balanceValidator.getSender(jsonArray)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(balanceValidator.getCreationDate(jsonArray)));

        Assert.assertEquals("It was expected the echoedMessage TEST",
                balanceValidator.getEchoedMessage(jsonArray), "TEST");

        Assert.assertTrue("It was expected the status REJECTED or DONE",
                balanceValidator.getStatus(jsonArray).equals("REJECTED") ||
                        balanceValidator.getStatus(jsonArray).equals("DONE"));

        Response responseGetBalancesById = balanceRequest.getBalancesByIdRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/balances/" + balanceValidator.getBalanceId(jsonArray));

        JSONObject jsonObjectBalanceId = HelpersValidator.getResponseBodyToJsonObject(responseGetBalancesById);

        Assert.assertEquals("It was expected the same balanceId",
                balanceValidator.getBalanceId(jsonArray), jsonObjectBalanceId.get(configurationsProperties.balanceId()));

        Response responseGetTransactionsById = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat(
                        "/"+balanceValidator.getTransactionsId(jsonArray)));

        JSONObject transactionsByIdJsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactionsById);

        Assert.assertEquals("It was expected the same",balanceValidator.getTransactionsId(jsonArray),
                transactionsByIdJsonObject.get(configurationsProperties.transactionId()));
    }

    @Test
    public void getBalances(){
        Response responseGetBalances = balanceRequest.getBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.balancesLimit100());

        Assert.assertEquals("It was expected the status code 206",206, responseGetBalances.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetBalances);

        Assert.assertTrue("It was expected the amount as integer value",
                balanceValidator.getBalancesAmount(jsonObject).stream().allMatch(x-> Integer.parseInt(x) > 0));

        Assert.assertTrue("It was expected the receiver has value",
                balanceValidator.getBalancesReceiver(jsonObject).stream().allMatch(StringUtils::isNotEmpty));

        Assert.assertTrue("It was expected the sender has value",
                balanceValidator.getBalancesSender(jsonObject).stream().allMatch(StringUtils::isNotEmpty));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(balanceValidator.getBalancesCreationDate(jsonObject)
                        .stream().findFirst().orElseThrow(()-> new NoSuchElementException("creation date are empty"))));

        Assert.assertTrue("It was expected the status REJECTED or DONE",
                balanceValidator.getBalancesStatus(jsonObject).stream().allMatch(
                        x-> x.equals("REJECTED") || x.equals("DONE")));

        Assert.assertTrue("It was expected the balanceId not empty",
                balanceValidator.getBalancesIds(jsonObject).stream().allMatch(StringUtils::isNotEmpty));
    }

    @Test
    public void getBalancesById(){
        Response responseGetBalancesById = balanceRequest.getBalancesByIdRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/balances/" + configurationsProperties.balancesById());

        Assert.assertEquals("It was expected the status code 200",200,
                responseGetBalancesById.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetBalancesById);

        Assert.assertTrue("It was expected an amount with integer value",
                Integer.parseInt(balanceValidator.getBalancesById(jsonObject)) > 0);

        Assert.assertTrue("It was expected an receiver with value",
                StringUtils.isNotEmpty(balanceValidator.getBalancesByIdReceiver(jsonObject)));

        Assert.assertTrue("It was expected an sender with value",
                StringUtils.isNotEmpty(balanceValidator.getBalancesByIdSender(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(balanceValidator.getBalancesByIdCreationDate(jsonObject)));

        Assert.assertTrue("It was expected not empty echoedMessage",
               StringUtils.isNotEmpty(balanceValidator.getBalancesByIdEchoedMessage(jsonObject)));

        Assert.assertTrue("It was expected the status REJECTED or DONE",
                balanceValidator.getBalancesByIdStatus(jsonObject).equals("REJECTED") ||
                        balanceValidator.getBalancesByIdStatus(jsonObject).equals("DONE"));

        Assert.assertEquals("It was expected the same balanceId",
                balanceValidator.getBalancesByIdBalanceId(jsonObject), configurationsProperties.balancesById());
    }

    @Test
    public void createBalanceWithTheSameSenderAndReceiver(){
        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balanceWithTheSameSenderAndReceiverRequestBody());

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue(balanceValidator.validateAllBalancesInRejectedStatus(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesAmountGraterThanZero(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesCreationDate(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalanceIdsNotEquals(jsonArray));

        Assert.assertTrue(balanceValidator.validateSameSenderAndReceiver(jsonArray));
    }

    @Test
    public void createBalanceWithoutIdsValidValuesAndBeneficiaries(){
        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balanceWithoutIdsValidValuesAndBeneficiariesRequestBody());

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue(balanceValidator.validateAllBalancesInDoneOrRejectedStatus(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesAmountGraterThanZero(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesCreationDate(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalanceIdsNotEquals(jsonArray));

        Assert.assertTrue(balanceValidator.validateSameSenderAndReceiver(jsonArray));
    }

    @Test
    public void createBalanceWithoutIdsAndInvalidAmountValueOnOneRecord(){
        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balanceWithoutIdsAndInvalidAmountValueOnOneRecordRequestBody());

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue(balanceValidator.validateAllBalancesInDoneOrRejectedStatus(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesAmountGraterThanZero(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesCreationDate(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalanceIdsNotEquals(jsonArray));

        Assert.assertTrue(balanceValidator.validateSameSenderAndReceiver(jsonArray));
    }

    @Test
    public void createBalanceWithCreatedIdAndValidValues(){
        String balanceId = "BALANCE_".concat(LocalDateTime.now().toString());

        Response response = balanceRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balanceRequest.balanceWithCreatedIdAndValidValuesRequestBody(balanceId));

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertTrue(balanceValidator.validateAllBalancesInDoneOrRejectedStatus(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesAmountGraterThanZero(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalancesCreationDate(jsonArray));

        Assert.assertTrue(balanceValidator.validateAllBalanceIdsNotEquals(jsonArray));

        Assert.assertTrue(balanceValidator.validateSameSenderAndReceiver(jsonArray));
    }
}