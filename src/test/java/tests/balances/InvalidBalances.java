package tests.balances;

import balances.BalancesRequest;
import balances.BalancesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;

import java.util.NoSuchElementException;

public class InvalidBalances {

    private static BalancesRequest balancesRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BalancesValidator balancesValidator;

    @BeforeClass
    public static void initClass(){
        balancesRequest = new BalancesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        balancesValidator = new BalancesValidator();
    }

    @Test
    public void createBalancesWithNoRequiredFields(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNoRequiredFieldsRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 3",
                balancesValidator.getMessage(jsonObject));

        //TODO: Bug found FN4OC – 387
    }

    @Test
    public void createBalancesWithNoAmountRequiredField(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNoRequiredFieldAmountRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                balancesValidator.getMessage(jsonObject));

        //TODO: Bug found FN4OC – 387
    }

    @Test
    public void createBalancesWithNullAmountRequiredField(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNullRequiredFieldAmountRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                balancesValidator.getMessage(jsonObject));

        //TODO: Bug found FN4OC – 387
    }

    @Test
    public void createBalancesWithNotValidAmountRequiredField(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNonValidRequiredFieldAmountRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Validation failed",
                balancesValidator.getMessage(jsonObject).contains("Cannot deserialize value"));
    }

    @Test
    public void createBalancesWithNotValidSenderRequiredField(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNoRequiredFieldSenderRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "com.mp.breakdowns.infra.exception.TransactionNotFoundException",
                balancesValidator.getMessage(jsonObject));

        //TODO: Bug found FN4OC – 387
    }

    @Test
    public void createBalancesWithNotValidReceiverRequiredField(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balancesWithNonValidRequiredFieldReceiverRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Validation failed",
                balancesValidator.getMessage(jsonObject).contains("BeneficiaryNotFoundException"));
    }

    @Test
    public void createBalancesWithAnInvalidTransactionId(){
        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balanceWithAnInvalidTransactionIdRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Validation failed",
                balancesValidator.getMessage(jsonObject).contains("TransactionNotFoundException"));
    }

    @Test
    public void createBalanceWithNotUniqueId(){

        Response getBalances = balancesRequest.getBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.balancesLimit100());

        JSONObject getBalancesJsonObject = HelpersValidator.getResponseBodyToJsonObject(getBalances);

        Assert.assertEquals("It was expected the status code 206",206, getBalances.statusCode());

        String balanceId = balancesValidator.getBalancesIds(getBalancesJsonObject).stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("Balance id not found"));

        Response response = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balanceWithNotUniqueIdRequestBody(balanceId));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                balancesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Validation failed",
                balancesValidator.getMessage(jsonObject).contains("BalanceIdAlreadyExistsException"));
    }
}

