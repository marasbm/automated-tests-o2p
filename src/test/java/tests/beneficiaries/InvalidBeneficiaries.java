package tests.beneficiaries;

import beneficiaries.BeneficiariesRequest;
import beneficiaries.BeneficiariesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

import java.time.LocalDateTime;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvalidBeneficiaries {

    private static BeneficiariesRequest beneficiariesRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesValidator beneficiariesValidator;

    @BeforeClass
    public static void initClass(){
        beneficiariesRequest = new BeneficiariesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesValidator = new BeneficiariesValidator();

        beneficiariesValidator = new BeneficiariesValidator();
    }

    @Test
    public void updateBeneficiaryStatusNotValid(){
        updateNotValidBeneficiaryStatus(
                configurationsProperties.notValid(), configurationsProperties.kycLoaded());
    }

    @Test
    public void updateBeneficiaryEmptyStatus(){
        updateNotValidBeneficiaryStatus("",configurationsProperties.kycLoaded());
    }

    @Test
    public void updateBeneficiaryEmptyKycStatus(){
        updateNotValidBeneficiaryStatus(configurationsProperties.notDeleted(), "");
    }

    @Test
    public void updateBeneficiaryStatusEndPointWithoutStatus(){
        Response response = beneficiariesRequest.putUpdateBeneficiaryStatusInvalidEndPoint(
                beneficiariesRequest.updateBeneficiaryStatusRequestBody(configurationsProperties.deleted(),""),
                configurationsProperties.beneficiariesUsersBaseUri().concat(
                        configurationsProperties.beneficiaryToUpdate().concat("/use")));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Not Found","Not Found",
                beneficiariesValidator.getError(jsonObject));
    }

    @Test
    public void updateBeneficiaryEmptyLegalName(){
        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryRequestBody("",emailToUpdate));

        Assert.assertEquals("It was expected the status code 422",422, response.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assert.assertEquals("It was expected required beneficiary name field",
                "required beneficiary name field", beneficiariesValidator.getDescription(jsonArray));

        //TODO: Possible bug or new US to implement the legalname or email validation
    }

    @Test
    public void updateBeneficiaryEmptyBody(){
        Response response = beneficiariesRequest.putUpdateBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),"");

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the Bad Request", "Bad Request",
                beneficiariesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Required request body is missing",
                beneficiariesValidator.getMessage(jsonObject).startsWith("Required request body is missing"));
    }

    @Test
    public void updateBeneficiaryWithoutBeneficiary(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiaryWithoutBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryRequestBody(legalNameToUpdate,emailToUpdate));

        Assert.assertEquals("It was expected the status code 405",405, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the error -> Method Not Allowed", "Method Not Allowed",
                beneficiariesValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Request method 'PUT' not supported",
                "Request method 'PUT' not supported", beneficiariesValidator.getMessage(jsonObject));
    }

    @Test
    public void updateBeneficiaryToMarketplace(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiaryToMarketPlace(
                beneficiariesRequest.updateBeneficiaryRequestBody(legalNameToUpdate,emailToUpdate));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the Not Found", "Not Found",
                beneficiariesValidator.getError(jsonObject));
    }

    @Test
    public void updateBeneficiaryInvalidBeneficiary(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiaryInvalidBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryRequestBody(legalNameToUpdate,emailToUpdate));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected 404 Invalid beneficiary id", "404 Invalid beneficiary id",
                beneficiariesValidator.getMessage(jsonObject));
    }

    @Test
    public void updateBeneficiaryInvalidEndPoint(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiaryInvalidEndPoint(
                beneficiariesRequest.updateBeneficiaryRequestBody(legalNameToUpdate,emailToUpdate));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());
    }

    private void updateNotValidBeneficiaryStatus(String statusToUpdate, String kycStatus) {
        Response response = beneficiariesRequest.putUpdateBeneficiaryStatus(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryStatusRequestBody(statusToUpdate,kycStatus));

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                beneficiariesValidator.getError(jsonObject));
    }
}