package tests.beneficiaries;

import beneficiaries.BeneficiariesRequest;
import beneficiaries.BeneficiariesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

import java.time.LocalDateTime;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidBeneficiaries {

    private static BeneficiariesRequest beneficiariesRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesValidator beneficiariesValidator;

    @BeforeClass
    public static void initClass(){
        beneficiariesRequest = new BeneficiariesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesValidator = new BeneficiariesValidator();
    }

    @Test
    public void updateBeneficiaryStatusDeleted(){
        updateBeneficiaryStatus(configurationsProperties.deleted(), configurationsProperties.kycRejected());
    }

    @Test
    public void updateBeneficiaryStatusActivated(){
        updateBeneficiaryStatusWithoutKycStatus(configurationsProperties.activated());
    }

    @Test
    public void updateBeneficiaryStatusActivatedAndKycValidated(){
        updateBeneficiaryStatus(configurationsProperties.activated(), configurationsProperties.kycValidated());
    }

    @Test
    public void updateBeneficiaryStatusNotActivated(){
        updateBeneficiaryStatus(configurationsProperties.notActivated(), configurationsProperties.kycLoaded());
    }

    @Test
    public void updateBeneficiary(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        String emailToUpdate = "Email".concat("Update_").concat(LocalDateTime.now().toString()).concat("@gmail.com");

        Response response = beneficiariesRequest.putUpdateBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryRequestBody(legalNameToUpdate,emailToUpdate));

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the same legalName", legalNameToUpdate,
                beneficiariesValidator.getLegalName(jsonObject));

        Assert.assertEquals("It was expected the same email", emailToUpdate,
                beneficiariesValidator.getEmail(jsonObject));
    }

    @Test
    public void getBeneficiariesValidateStatusAndKycStatus(){
        Response response = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue(beneficiariesValidator.validateAllStatus(jsonObject));

        Assert.assertTrue(beneficiariesValidator.validateAllkycStatus(jsonObject));
    }

    @Test
    public void getBeneficiaryByIdValidateStatusAndKycStatus(){
        Response response = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        String beneficiaryId = beneficiariesValidator.getItemsBeneficiaryId(jsonObject);

        Response responseBeneficiaryId = beneficiariesRequest.getBeneficiaryById(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(), beneficiaryId);

        Assert.assertEquals("It was expected the status code 200",200, responseBeneficiaryId.statusCode());

        JSONObject jsonObjectBeneficiaryId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryId);

        Assert.assertTrue(beneficiariesValidator.validateBeneficiaryByIdStatus(jsonObjectBeneficiaryId));

        Assert.assertTrue(beneficiariesValidator.validateBeneficiaryByIdkycStatus(jsonObjectBeneficiaryId));
    }

    @Test
    public void updateBeneficiaryOnlyFieldLegalName(){
        String legalNameToUpdate = "LegalName_".concat("Update_").concat(LocalDateTime.now().toString());

        Response responseGetBeneficiaryByIdBeforeUpdate = beneficiariesRequest.getBeneficiaryById(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiaryToUpdate());

        JSONObject jObjectGetBeneficiaryByIdBeforeUpdate = HelpersValidator.getResponseBodyToJsonObject(
                responseGetBeneficiaryByIdBeforeUpdate);

        String getStatusBeforeUpdate = beneficiariesValidator.getStatus(jObjectGetBeneficiaryByIdBeforeUpdate);

        Response responseUpdateBeneficiary = beneficiariesRequest.putUpdateBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryOnlyFieldLegalName(legalNameToUpdate));

        JSONObject jsonObjectUpdate = HelpersValidator.getResponseBodyToJsonObject(responseUpdateBeneficiary);

        Assert.assertEquals("It was expected the same legalName", legalNameToUpdate,
                beneficiariesValidator.getLegalName(jsonObjectUpdate));

        Assert.assertEquals("It was expected the same status", getStatusBeforeUpdate,
                beneficiariesValidator.getStatus(jObjectGetBeneficiaryByIdBeforeUpdate));
    }

    @Test
    public void searchByCompleteLegalName(){
        Response responseGetBeneficiaries = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetBeneficiaries);

        Assert.assertEquals("It was expected the status code 206",206, responseGetBeneficiaries.statusCode());

        String legalName = beneficiariesValidator.getItemsLegalName(jsonObject);

        Response responseSearchLegalName = beneficiariesRequest.getBeneficiariesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/beneficiaries?legalName="+legalName+"");

        Assert.assertEquals("It was expected the status code 200",200, responseSearchLegalName.statusCode());

        JSONObject jsonObjectSearchLegalName = HelpersValidator.getResponseBodyToJsonObject(responseSearchLegalName);

        String legalNameAfterSearch = beneficiariesValidator.getItemsLegalName(jsonObjectSearchLegalName);

        Assert.assertEquals("It was expected the same", legalName, legalNameAfterSearch);
    }

    @Test
    public void searchByPartialLegalName(){
        Response responseGetBeneficiaries = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetBeneficiaries);

        Assert.assertEquals("It was expected the status code 206",206, responseGetBeneficiaries.statusCode());

        String partialLegalName = beneficiariesValidator.getItemsLegalName(jsonObject).substring(0,4);

        Response responseSearchLegalName = beneficiariesRequest.getBeneficiariesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/beneficiaries?legalName="+partialLegalName+"");

        Assert.assertEquals("It was expected the status code 200",200, responseSearchLegalName.statusCode());

        JSONObject jsonObjectSearchLegalName = HelpersValidator.getResponseBodyToJsonObject(responseSearchLegalName);

        Assert.assertTrue("It was expected the partialLegalName in List"
                ,beneficiariesValidator.validatePartialLegalnameInList(jsonObjectSearchLegalName,partialLegalName));
    }

    @Test
    public void searchByCompleteEmail(){
        Response responseGetBeneficiaries = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetBeneficiaries);

        Assert.assertEquals("It was expected the status code 206",206, responseGetBeneficiaries.statusCode());

        String email = beneficiariesValidator.getItemsEmail(jsonObject);

        Response responseSearchEmail = beneficiariesRequest.getBeneficiariesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/beneficiaries?email="+email+"");

        Assert.assertEquals("It was expected the status code 200",200, responseSearchEmail.statusCode());

        JSONObject jsonObjectSearchEmail = HelpersValidator.getResponseBodyToJsonObject(responseSearchEmail);

        String EmailAfterSearch = beneficiariesValidator.getItemsEmail(jsonObjectSearchEmail);

        Assert.assertEquals("It was expected the same", email, EmailAfterSearch);
    }

    @Test
    public void searchByPartialEmail(){
        Response responseGetBeneficiaries = beneficiariesRequest.getBeneficiaries(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken());

        JSONObject jsonObjectResponseGetBeneficiaries = HelpersValidator.getResponseBodyToJsonObject(responseGetBeneficiaries);

        Assert.assertEquals("It was expected the status code 206",206, responseGetBeneficiaries.statusCode());

        String partialEmail = beneficiariesValidator.getItemsEmail(jsonObjectResponseGetBeneficiaries).substring(0,20);

        Response responseSearchEmail = beneficiariesRequest.getBeneficiariesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                "/beneficiaries?email="+partialEmail+"");

        Assert.assertEquals("It was expected the status code 200",200, responseSearchEmail.statusCode());

        JSONObject jsonObjectSearchEmail = HelpersValidator.getResponseBodyToJsonObject(responseSearchEmail);

        Assert.assertTrue("It was expected the partialEmail in List"
                ,beneficiariesValidator.validatePartialEmailInList(jsonObjectSearchEmail,partialEmail));
    }

    private void updateBeneficiaryStatus(String statusToUpdate, String kycStatusToUpdate) {
        Response response = beneficiariesRequest.putUpdateBeneficiaryStatus(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryStatusRequestBody(statusToUpdate, kycStatusToUpdate));

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Response responseGetBeneficiaryById = beneficiariesRequest.getBeneficiaryById(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiaryToUpdate());

        JSONObject jObjectGetBeneficiaryById = HelpersValidator.getResponseBodyToJsonObject(responseGetBeneficiaryById);

        Assert.assertEquals("It was expected the same status", statusToUpdate,
                beneficiariesValidator.getStatus(jObjectGetBeneficiaryById));

        Assert.assertTrue("It was expected a not empty email", StringUtils.isNotEmpty(
                beneficiariesValidator.getEmail(jObjectGetBeneficiaryById)));

        Assert.assertEquals("It was expected the same beneficiaryId",
                configurationsProperties.beneficiaryToUpdate(),
                beneficiariesValidator.getBeneficiaryId(jObjectGetBeneficiaryById));

        Assert.assertTrue("It was expected a not empty legal name", StringUtils.isNotEmpty(
                beneficiariesValidator.getLegalName(jObjectGetBeneficiaryById)));
    }

    private void updateBeneficiaryStatusWithoutKycStatus(String statusToUpdate) {
        Response response = beneficiariesRequest.putUpdateBeneficiaryStatus(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                beneficiariesRequest.updateBeneficiaryStatusRequestBody(
                        statusToUpdate, configurationsProperties.kycValidated()));

        Response responseBeneficiaryId = beneficiariesRequest.getBeneficiaryById(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiaryToUpdate());

        Assert.assertEquals("It was expected the status code 200",200, responseBeneficiaryId.statusCode());

        JSONObject jsonObjectBeneficiaryId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryId);

        String kycStatus = beneficiariesValidator.getBeneficiaryByIdKycStatusValue(jsonObjectBeneficiaryId);

        if(kycStatus.equals("KYC_VALIDATED")){
            Assert.assertEquals("It was expected the status code 200",200, response.statusCode());
        }
        else{
            Assert.assertEquals("It was expected the status code 501",501, response.statusCode());
        }
    }
}