package tests.runner.payment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.paymentpageauthorization.InvalidPaymentPageAuthorization;
import tests.paymentpageauthorization.ValidPaymentPageAuthorization;

@RunWith(Suite.class)
@Suite.SuiteClasses({ValidPaymentPageAuthorization.class, InvalidPaymentPageAuthorization.class})
public class PaymentPageAuthorizationRunner { }