package tests.runner.payment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.paymentpagecapture.InvalidPaymentPageCapture;
import tests.paymentpagecapture.ValidPaymentPageCapture;


@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidPaymentPageCapture.class, ValidPaymentPageCapture.class})
public class PaymentPageCaptureRunner { }
