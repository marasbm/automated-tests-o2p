package tests.runner.payment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({PaymentPageAuthorizationRunner.class, PaymentPageCaptureRunner.class})
public class PaymentPageRunner { }
