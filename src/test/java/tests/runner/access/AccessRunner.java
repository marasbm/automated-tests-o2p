package tests.runner.access;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.apisecurity.ApiSecurity;
import tests.security.Security;

@RunWith(Suite.class)
@Suite.SuiteClasses({Security.class, ApiSecurity.class})
public class AccessRunner { }
