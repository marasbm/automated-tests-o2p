package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.transactions.InvalidTransactions;
import tests.transactions.ValidTransactions;

@RunWith(Suite.class)
@Suite.SuiteClasses({ValidTransactions.class, InvalidTransactions.class})
public class TransactionsRunner { }
