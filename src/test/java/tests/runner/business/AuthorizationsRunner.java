package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.authorizations.InvalidAuthorizations;
import tests.authorizations.ValidAuthorizations;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidAuthorizations.class, ValidAuthorizations.class,})
public class AuthorizationsRunner { }
