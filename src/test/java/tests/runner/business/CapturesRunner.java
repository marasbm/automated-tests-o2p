package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.captures.InvalidCaptures;
import tests.captures.validCaptures;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidCaptures.class, validCaptures.class,})
public class CapturesRunner { }
