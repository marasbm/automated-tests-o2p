package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.balances.InvalidBalances;
import tests.balances.ValidBalances;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidBalances.class, ValidBalances.class,})
public class BalancesRunner { }
