package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.breakdown.InvalidBreakdown;
import tests.breakdown.ValidBreakdown;

@RunWith(Suite.class)
@Suite.SuiteClasses({ValidBreakdown.class, InvalidBreakdown.class})
public class BreakdownRunner { }
