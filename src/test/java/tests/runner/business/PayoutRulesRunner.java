package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.payoutrules.InvalidPayoutRules;
import tests.payoutrules.ValidPayoutRules;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidPayoutRules.class, ValidPayoutRules.class})
public class PayoutRulesRunner { }
