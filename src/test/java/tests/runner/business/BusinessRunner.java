package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BalancesRunner.class, BreakdownRunner.class, CapturesRunner.class, PayoutRulesRunner.class,
RefundsRunner.class, AuthorizationsRunner.class, TransactionsRunner.class})
public class BusinessRunner { }