package tests.runner.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.refunds.InvalidRefunds;
import tests.refunds.ValidRefunds;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidRefunds.class, ValidRefunds.class})
public class RefundsRunner { }
