package tests.runner.smoketests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.runner.Listener.StopOnFailureSuite;
import tests.security.Security;
import tests.smoketests.SmokeTests;

@RunWith(StopOnFailureSuite.class)
@Suite.SuiteClasses({Security.class, SmokeTests.class})
public class SmokeTestsRunner { }
