package tests.runner.beneficiaries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.beneficiaries.InvalidBeneficiaries;
import tests.beneficiaries.ValidBeneficiaries;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidBeneficiaries.class, ValidBeneficiaries.class,})
public class BeneficiariesBackOfficeRunner { }
