package tests.runner.beneficiaries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.beneficiariessmoney.InvalidBeneficiariesSmoney;
import tests.beneficiariessmoney.ValidBeneficiariesSmoney;

@RunWith(Suite.class)
@Suite.SuiteClasses({ValidBeneficiariesSmoney.class, InvalidBeneficiariesSmoney.class,})
public class SmoneyBeneficiariesRunner {
}
