package tests.runner.beneficiaries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BeneficiariesBackOfficeRunner.class, BeneficiariesAndMarketplaceUsersRunner.class,
        AccountAmountRunner.class, SmoneyBeneficiariesRunner.class})
public class BeneficiariesRunner { }
