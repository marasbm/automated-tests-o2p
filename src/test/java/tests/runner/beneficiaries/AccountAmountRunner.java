package tests.runner.beneficiaries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.accountamount.InvalidAccountAmount;
import tests.accountamount.ValidAccountAmount;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidAccountAmount.class, ValidAccountAmount.class})
public class AccountAmountRunner { }
