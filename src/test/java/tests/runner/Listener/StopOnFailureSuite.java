package tests.runner.Listener;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;

public class StopOnFailureSuite extends Suite {

    public StopOnFailureSuite(Class<?> c, Class<?>[] suiteClasses) throws InitializationError {
        super(c, suiteClasses);
    }

    public StopOnFailureSuite(Class<?> c) throws InitializationError {
        super(c, c.getAnnotation(SuiteClasses.class).value());
    }

    @Override
    public void run(RunNotifier runNotifier) {
        runNotifier.addListener(new FailureListener(runNotifier));
        super.run(runNotifier);
    }
}