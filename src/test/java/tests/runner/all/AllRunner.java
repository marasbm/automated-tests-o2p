package tests.runner.all;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.runner.access.AccessRunner;
import tests.runner.beneficiaries.BeneficiariesRunner;
import tests.runner.business.BusinessRunner;
import tests.runner.payment.PaymentPageRunner;
import tests.runner.smoketests.SmokeTestsRunner;
import tests.smoketests.SmokeTests;


@RunWith(Suite.class)
@Suite.SuiteClasses({AccessRunner.class, SmokeTests.class, BeneficiariesRunner.class, BusinessRunner.class,
        PaymentPageRunner.class})
public class AllRunner { }
