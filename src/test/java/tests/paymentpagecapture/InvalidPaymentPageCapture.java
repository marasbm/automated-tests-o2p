package tests.paymentpagecapture;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import paymentpagecapture.PaymentPageCaptureRequest;
import paymentpagevalidator.PaymentPageValidator;
import token.GeneratedTokenProperties;

public class InvalidPaymentPageCapture {

    private static PaymentPageCaptureRequest paymentPageCaptureRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private PaymentPageValidator paymentPageValidator;

    @BeforeClass
    public static void initClass(){
        paymentPageCaptureRequest = new PaymentPageCaptureRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        paymentPageValidator = new PaymentPageValidator();
    }

    @Test
    public void invalidRequestPaymentPageUrlNoRequiredFieldAmount(){
        Response response = paymentPageCaptureRequest.paymentPageCaptureRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageCaptureRequest.paymentPageCaptureRequestBodyNoRequiredFieldAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected missing: amount",
                "The following parameters are missing: amount",
                paymentPageValidator.getDescription(jsonObject));

        Assert.assertEquals("It was expected MISSING_PARAM", "MISSING_PARAM",
                paymentPageValidator.getType(jsonObject));
    }

    @Test
    public void invalidRequestPaymentPageUrlCaptureGratedThanAmount(){
        Response response = paymentPageCaptureRequest.paymentPageCaptureRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageCaptureRequest.paymentPageCaptureRequestBodyCaptureGratedThanAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("The capture amount must be equal to breakdown transfers",
                paymentPageValidator.getMessage(jsonObject));
    }

    @Test
    public void invalidRequestPaymentPageUrlCaptureMinorThanAmount(){
        Response response = paymentPageCaptureRequest.paymentPageCaptureRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageCaptureRequest.paymentPageCaptureRequestBodyCaptureMinorThanAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("The capture amount must be equal to breakdown transfers",
                paymentPageValidator.getMessage(jsonObject));
    }
}