package tests.paymentpagecapture;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import paymentpagecapture.PaymentPageCaptureRequest;
import paymentpagevalidator.PaymentPageValidator;
import token.GeneratedTokenProperties;

public class ValidPaymentPageCapture {

    private static PaymentPageCaptureRequest paymentPageCaptureRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private PaymentPageValidator paymentPageValidator;

    @BeforeClass
    public static void initClass(){
        paymentPageCaptureRequest = new PaymentPageCaptureRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        paymentPageValidator = new PaymentPageValidator();
    }

    @Test
    public void securityPaymentPageCaptureUrl(){
        Response response = paymentPageCaptureRequest.paymentPageCaptureRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageCaptureRequest.paymentPageCaptureRequestBody(configurationsProperties.validBeneficiary()));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        Assert.assertTrue("It was expected the amount value",
                StringUtils.isNotEmpty(paymentPageValidator.getAmount(jsonObject)));

        Assert.assertEquals("It was expected the same value", paymentPageValidator.getAmount(jsonObject),
                paymentPageValidator.getCaptureAmount(jsonObject));

        Assert.assertEquals("It was expected the same amount",
                Integer.parseInt(paymentPageValidator.getAmount(jsonObject)),
                paymentPageValidator.transfersTotalAmount(jsonObject));

        Assert.assertTrue("It was expected the orderId value",
                StringUtils.isNotEmpty(paymentPageValidator.getOrderId(jsonObject)));

        Assert.assertTrue("It was expected the transactionId value",
                StringUtils.isNotEmpty(paymentPageValidator.getTransactionId(jsonObject)));

        Assert.assertTrue("It was expected the responseUrl value",
                StringUtils.isNotEmpty(paymentPageValidator.getResponseUrl(jsonObject)));

        Assert.assertTrue("It was expected the customerId value",
                StringUtils.isNotEmpty(paymentPageValidator.getCustomerId(jsonObject)));

        Assert.assertEquals("It was expected currency EUR", "EUR",
                paymentPageValidator.getCurrency(jsonObject));

        Assert.assertTrue("It was expected the web page url with token value",
                paymentPageValidator.getPaymentPageUrl(jsonObject).contains("token="));

        Assert.assertTrue("It was expected the returnUrlValue value",
                StringUtils.isNotEmpty(paymentPageValidator.getReturnUrl(jsonObject)));

        Assert.assertFalse(paymentPageValidator.getPaymentPageUrl(jsonObject).contains("transactionId"));

        Assert.assertTrue("It was expected the paymentId value",
                StringUtils.isNotEmpty(paymentPageValidator.getPaymentId(jsonObject)));

        Assert.assertTrue("It was expected the breakdownId value",
                StringUtils.isNotEmpty(paymentPageValidator.getBreakdownId(jsonObject)));

        Assert.assertTrue("It was expected the subOrder value",
                paymentPageValidator.validateTransfersSubOrderIdNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the beneficiary value",
                paymentPageValidator.validateBeneficiaryNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the transferId values are different",
                paymentPageValidator.validateAllTransferIdNotEquals(jsonObject));
    }
}