package tests.accountamount;

import accountamount.AccountAmountRequest;
import accountamount.AccountAmountValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;

public class InvalidAccountAmount {

    private static AccountAmountRequest accountAmountRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private AccountAmountValidator accountAmountValidator;

    @BeforeClass
    public static void initClass(){
        accountAmountRequest = new AccountAmountRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        accountAmountValidator = new AccountAmountValidator();
    }

    @Test
    public void getAccountAmountForInvalidBeneficiary(){
        Response response = accountAmountRequest.getAccountAmount(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidBeneficiary());

        Assert.assertEquals("It was expected the status code 500",500, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected a bad request",
                "Bad request to /getAccountAmount on SMoney Service",
                accountAmountValidator.getMessage(jsonObject));
    }
}
