package tests.accountamount;

import accountamount.AccountAmountRequest;
import accountamount.AccountAmountValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;


public class ValidAccountAmount {

    private static AccountAmountRequest accountAmountRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private AccountAmountValidator accountAmountValidator;

    @BeforeClass
    public static void initClass(){
        accountAmountRequest = new AccountAmountRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        accountAmountValidator = new AccountAmountValidator();
    }

    @Test
    public void getAccountAmountForBeneficiary(){
        Response response = accountAmountRequest.getAccountAmount(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary());

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected an amount grater than zero",
                Integer.parseInt(accountAmountValidator.getAmount(jsonObject)) > 0) ;
    }
}