package tests.beneficiariesandmarketplaceusers;

import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersRequest;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvalidBeneficiariesAndMarketplaceUsers {

    private static BeneficiariesAndMarketplaceUsersRequest beneficiariesAndMarketplaceUsersRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    @BeforeClass
    public static void initClass(){
        beneficiariesAndMarketplaceUsersRequest = new BeneficiariesAndMarketplaceUsersRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Test
    public void getUsersFromBeneficiaryNotFound(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidBeneficiary());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());
    }

    @Test
    public void getUsersFromMarketplaceNotFound(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.invalidMarketplace());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());
    }

    @Test
    public void getUserFromBeneficiaryByUserIdNotFound(){
        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromBeneficiaryByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.validBeneficiary(), "123456");

        Assert.assertEquals("It was expected the status code 404",404,
                responseBeneficiaryByUserId.statusCode());
    }

    @Test
    public void getUserFromMarketplaceByUserIdNotFound(){
        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromMarketplaceByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.marketplaceswynd(), "fff6b192-408a-43fb-a722-2185768ce1b9");

        Assert.assertEquals("It was expected the status code 404",404,
                responseBeneficiaryByUserId.statusCode());
    }
}