package tests.beneficiariesandmarketplaceusers;

import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersRequest;
import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.*;
import token.GeneratedTokenProperties;


public class InvalidBeneficiariesAndMarketplaceCreateUsers {

    private static BeneficiariesAndMarketplaceUsersRequest beneficiariesAndMarketplaceUsersRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesAndMarketplaceUsersValidator beneficiariesAndMarketplaceUsersValidator;

    private String email;

    @BeforeClass
    public static void initClass(){
        beneficiariesAndMarketplaceUsersRequest = new BeneficiariesAndMarketplaceUsersRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesAndMarketplaceUsersValidator = new BeneficiariesAndMarketplaceUsersValidator();

        email = HelpersValidator.generateSaltString().concat("@email.com");
    }

    @Test
    public void createBeneficiaryUserWithAlreadyExistsId(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.validBeneficiary());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObject);

        Response responseBeneficiaryUserWithAlreadyExistsId = beneficiariesAndMarketplaceUsersRequest.createBeneficiaryUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBodyWithAlreadyExistsId(email,userId));

        Assert.assertEquals("It was expected the status code 400",400,
                responseBeneficiaryUserWithAlreadyExistsId.statusCode());

        JSONObject jsonObjectBeneficiaryUserWithAlreadyExistsId = HelpersValidator.getResponseBodyToJsonObject(
                responseBeneficiaryUserWithAlreadyExistsId);

        Assert.assertEquals("It was expected Bad request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectBeneficiaryUserWithAlreadyExistsId));

        Assert.assertEquals("It was expected message -> User ID already exists", "User ID already exists",
                beneficiariesAndMarketplaceUsersValidator.getMessage(jsonObjectBeneficiaryUserWithAlreadyExistsId));
    }

    @Test
    public void createBeneficiaryUserWithInvalidBeneficiary(){
        Response response = beneficiariesAndMarketplaceUsersRequest.invalidCreationBeneficiaryUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidBeneficiary(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBody(email));

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObject));
    }

    @Test
    public void createMarketplaceUserWithAlreadyExistsId(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromMarketplace(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.marketplaceswynd());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObject);

        Response responseMarketplaceUserWithAlreadyExistsId = beneficiariesAndMarketplaceUsersRequest.createMarketPlaceUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBodyWithAlreadyExistsId(email,userId));

        Assert.assertEquals("It was expected the status code 400", 400,
                responseMarketplaceUserWithAlreadyExistsId.statusCode());

        JSONObject jsonObjectBeneficiaryUserWithAlreadyExistsId = HelpersValidator.getResponseBodyToJsonObject(
                responseMarketplaceUserWithAlreadyExistsId);

        Assert.assertEquals("It was expected Bad request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectBeneficiaryUserWithAlreadyExistsId));

        Assert.assertEquals("It was expected message -> User ID already exists", "User ID already exists",
                beneficiariesAndMarketplaceUsersValidator.getMessage(jsonObjectBeneficiaryUserWithAlreadyExistsId));
    }

    @Test
    public void createMarketplaceUserWithInvalidMarketplace(){
        Response response = beneficiariesAndMarketplaceUsersRequest.invalidCreationBeneficiaryUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidMarketplace(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBody(email));

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObject));
    }

    @Test
    public void createBeneficiaryUserWithEmptyBody(){
        Response responseBeneficiaryUserWithEmptyBody = beneficiariesAndMarketplaceUsersRequest.createBeneficiaryUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary(),"");

        Assert.assertEquals("It was expected the status code 400",400,
                responseBeneficiaryUserWithEmptyBody.statusCode());

        JSONObject jsonObjectBeneficiaryUserWithEmptyBody = HelpersValidator.getResponseBodyToJsonObject(
                responseBeneficiaryUserWithEmptyBody);

        Assert.assertEquals("It was expected Bad request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectBeneficiaryUserWithEmptyBody));

        Assert.assertTrue("It was expected starts with Required request body is missing",
                beneficiariesAndMarketplaceUsersValidator.getMessage(jsonObjectBeneficiaryUserWithEmptyBody)
                        .startsWith("Required request body is missing"));
    }

    @Test
    public void createMarketplaceUserWithEmptyBody(){
        Response responseMarketplaceUserWithEmptyBody = beneficiariesAndMarketplaceUsersRequest.createMarketPlaceUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.marketplaceswynd(),"");

        Assert.assertEquals("It was expected the status code 400",400,
                responseMarketplaceUserWithEmptyBody.statusCode());

        JSONObject jsonObjectMarketplaceUserWithEmptyBody = HelpersValidator.getResponseBodyToJsonObject(
                responseMarketplaceUserWithEmptyBody);

        Assert.assertEquals("It was expected Bad request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectMarketplaceUserWithEmptyBody));

        Assert.assertTrue("It was expected starts with Required request body is missing",
                beneficiariesAndMarketplaceUsersValidator.getMessage(jsonObjectMarketplaceUserWithEmptyBody)
                        .startsWith("Required request body is missing"));
    }

    @Test
    public void createMarketplaceUserInBeneficiariesEndPoint(){
        Response responseMarketplaceUserInBeneficiariesEndPoint = beneficiariesAndMarketplaceUsersRequest.invalidCreationUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBody(email),
                configurationsProperties.beneficiariesUsersBaseUri().concat(configurationsProperties.marketplaceswynd())
                        .concat(configurationsProperties.users()));

        Assert.assertEquals("It was expected the status code 404",404,
                responseMarketplaceUserInBeneficiariesEndPoint.statusCode());

        JSONObject jsonObjectMarketplaceUserInBeneficiariesEndPoint = HelpersValidator.getResponseBodyToJsonObject(
                responseMarketplaceUserInBeneficiariesEndPoint);

        Assert.assertEquals("It was expected Not Found", "Not Found",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectMarketplaceUserInBeneficiariesEndPoint));
    }

    @Test
    public void createBeneficiaryUserInMarketplaceEndPoint(){
        Response responseMarketplaceUserInBeneficiariesEndPoint = beneficiariesAndMarketplaceUsersRequest.invalidCreationUser(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBody(email),
                configurationsProperties.integrationEnvironment().concat(configurationsProperties.marketplacesBaseUri())
                .concat(configurationsProperties.validBeneficiary())
                        .concat(configurationsProperties.users()));

        Assert.assertEquals("It was expected the status code 400",400,
                responseMarketplaceUserInBeneficiariesEndPoint.statusCode());

        JSONObject jsonObjectMarketplaceUserInBeneficiariesEndPoint = HelpersValidator.getResponseBodyToJsonObject(
                responseMarketplaceUserInBeneficiariesEndPoint);

        Assert.assertEquals("It was expected Bad request", "Bad Request",
                beneficiariesAndMarketplaceUsersValidator.getError(jsonObjectMarketplaceUserInBeneficiariesEndPoint));

        Assert.assertEquals("It was expected Entity not found", "Entity not found",
                beneficiariesAndMarketplaceUsersValidator.getMessage(jsonObjectMarketplaceUserInBeneficiariesEndPoint));
    }
}