package tests.beneficiariesandmarketplaceusers;

import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersRequest;
import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import helpers.Mongo;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.*;
import token.GeneratedTokenProperties;

import java.net.UnknownHostException;

public class ValidBeneficiariesAndMarketplaceCreateUsers {

    private static BeneficiariesAndMarketplaceUsersRequest beneficiariesAndMarketplaceUsersRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesAndMarketplaceUsersValidator beneficiariesAndMarketplaceUsersValidator;

    private String email;

    @BeforeClass
    public static void initClass(){
        beneficiariesAndMarketplaceUsersRequest = new BeneficiariesAndMarketplaceUsersRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesAndMarketplaceUsersValidator = new BeneficiariesAndMarketplaceUsersValidator();

        email = HelpersValidator.generateSaltString().concat("@email.com");
    }

    @After
    public void finalTest() throws UnknownHostException {
        Mongo.deleteCreatedUser(email);
    }

    @Test
    public void createBeneficiaryUser(){
        Response response = beneficiariesAndMarketplaceUsersRequest.createBeneficiaryUser(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.validBeneficiary(),
                beneficiariesAndMarketplaceUsersRequest.createUserRequestBody(email));

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdFirstName(jsonObject)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdLastName(jsonObject)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdPhone(jsonObject)));

        Assert.assertTrue("It was expected a not empty user id", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObject)));

        Assert.assertEquals("It was expected a not empty email", email,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObject));

        Assert.assertTrue("It was expected the parse dateTime value", HelpersValidator
                .validateDateFormatWithMills(
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdCreationDate(jsonObject)));

        Assert.assertEquals("It was expected the status ACTIVATED",
                beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObject), configurationsProperties.activated());

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObject);

        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromBeneficiaryByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.validBeneficiary(), userId);

        Assert.assertEquals("It was expected the status code 200",200,
                responseBeneficiaryByUserId.statusCode());

        JSONObject jsonObjectGetUserByUserId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryByUserId);

        Assert.assertEquals("It was expected the same email", email,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObjectGetUserByUserId));
    }

    @Test
    public void createMarketplaceUser(){
        Response response = beneficiariesAndMarketplaceUsersRequest.createMarketPlaceUser(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.marketplaceswynd(), beneficiariesAndMarketplaceUsersRequest
                        .createUserRequestBody(email));

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdFirstName(jsonObject)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdLastName(jsonObject)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdPhone(jsonObject)));

        Assert.assertTrue("It was expected a not empty user id", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObject)));

        Assert.assertEquals("It was expected a not empty email", email,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObject));

        Assert.assertTrue("It was expected the parse dateTime value", HelpersValidator
                .validateDateFormatWithMills(beneficiariesAndMarketplaceUsersValidator.getUserByIdCreationDate(jsonObject)));

        Assert.assertEquals("It was expected the status ACTIVATED",
                beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObject), configurationsProperties.activated());

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObject);

        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromMarketplaceByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.marketplaceswynd(), userId);

        Assert.assertEquals("It was expected the status code 200",200,
                responseBeneficiaryByUserId.statusCode());

        JSONObject jsonObjectGetUserByUserId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryByUserId);

        Assert.assertEquals("It was expected the same email", email,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObjectGetUserByUserId));
    }
}