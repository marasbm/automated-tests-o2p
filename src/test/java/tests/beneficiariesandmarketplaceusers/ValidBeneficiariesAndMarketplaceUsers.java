package tests.beneficiariesandmarketplaceusers;

import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersRequest;
import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidBeneficiariesAndMarketplaceUsers {

    private static BeneficiariesAndMarketplaceUsersRequest beneficiariesAndMarketplaceUsersRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BeneficiariesAndMarketplaceUsersValidator beneficiariesAndMarketplaceUsersValidator;

    @BeforeClass
    public static void initClass(){
        beneficiariesAndMarketplaceUsersRequest = new BeneficiariesAndMarketplaceUsersRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        beneficiariesAndMarketplaceUsersValidator = new BeneficiariesAndMarketplaceUsersValidator();
    }

    @Test
    public void getUsersFromBeneficiary(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected the total grater than zero",
                beneficiariesAndMarketplaceUsersValidator.getTotalElements(jsonObject) > 0);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getFirstName(jsonObject)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getLastName(jsonObject)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getPhone(jsonObject)));

        Assert.assertTrue("It was expected a not empty user id", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObject)));

        Assert.assertTrue("It was expected a not empty email", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getEmail(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(beneficiariesAndMarketplaceUsersValidator.getCreationDate(jsonObject)));

        Assert.assertTrue("It was expected one off this status",
                beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.activated())||
                beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.deleted()) ||
                beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.notActivated()));
    }

    @Test
    public void getUserFromBeneficiaryByUserId(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObjectGetUsers = HelpersValidator.getResponseBodyToJsonObject(response);

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObjectGetUsers);

        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromBeneficiaryByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.validBeneficiary(), userId);

        Assert.assertEquals("It was expected the status code 200",200,
                responseBeneficiaryByUserId.statusCode());

        JSONObject jsonObjectGetUserByUserId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryByUserId);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdFirstName(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdLastName(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdPhone(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty email", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdCreationDate(jsonObjectGetUserByUserId)));

        Assert.assertEquals("It was expected the same userId",userId,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObjectGetUserByUserId));

        Assert.assertTrue("It was expected one off this status",
                beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                        .equals(configurationsProperties.activated())||
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                                .equals(configurationsProperties.deleted()) ||
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                                .equals(configurationsProperties.notActivated()));
    }

    @Test
    public void getUsersFromMarketplace(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromMarketplace(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.marketplaceswynd());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertTrue("It was expected the total grater than zero",
                beneficiariesAndMarketplaceUsersValidator.getTotalElements(jsonObject) > 0);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getFirstName(jsonObject)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getLastName(jsonObject)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getPhone(jsonObject)));

        Assert.assertTrue("It was expected a not empty user id", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObject)));

        Assert.assertTrue("It was expected a not empty email", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getEmail(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(beneficiariesAndMarketplaceUsersValidator.getCreationDate(jsonObject)));

        Assert.assertTrue("It was expected one off this status",
                beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.activated())||
                        beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.deleted()) ||
                        beneficiariesAndMarketplaceUsersValidator.getStatus(jsonObject).equals(configurationsProperties.notActivated()));
    }

    @Test
    public void getUserFromMarketplaceByUserId(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromMarketplace(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.marketplaceswynd());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        JSONObject jsonObjectGetUsers = HelpersValidator.getResponseBodyToJsonObject(response);

        String userId = beneficiariesAndMarketplaceUsersValidator.getUserId(jsonObjectGetUsers);

        Response responseBeneficiaryByUserId = beneficiariesAndMarketplaceUsersRequest.getUserFromMarketplaceByUserId(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.marketplaceswynd(), userId);

        Assert.assertEquals("It was expected the status code 200",200,
                responseBeneficiaryByUserId.statusCode());

        JSONObject jsonObjectGetUserByUserId = HelpersValidator.getResponseBodyToJsonObject(responseBeneficiaryByUserId);

        Assert.assertTrue("It was expected a not empty first name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdFirstName(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty last name", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdLastName(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty phone", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdPhone(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected a not empty email", StringUtils.isNotEmpty(
                beneficiariesAndMarketplaceUsersValidator.getUserByIdEmail(jsonObjectGetUserByUserId)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdCreationDate(jsonObjectGetUserByUserId)));

        Assert.assertEquals("It was expected the same userId",userId,
                beneficiariesAndMarketplaceUsersValidator.getUserByIdUserId(jsonObjectGetUserByUserId));

        Assert.assertTrue("It was expected one off this status",
                beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                        .equals(configurationsProperties.activated())||
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                                .equals(configurationsProperties.deleted()) ||
                        beneficiariesAndMarketplaceUsersValidator.getUserByIdStatus(jsonObjectGetUserByUserId)
                                .equals(configurationsProperties.notActivated()));
    }
}


