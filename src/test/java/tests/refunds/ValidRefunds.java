package tests.refunds;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import refunds.RefundsRequest;
import refunds.RefundsValidator;
import token.GeneratedTokenProperties;

import java.util.NoSuchElementException;

public class ValidRefunds {

    private static RefundsRequest refundsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private static RefundsValidator refundsValidator;


    @BeforeClass
    public static void initClass(){
        refundsRequest = new RefundsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        refundsValidator = new RefundsValidator();
    }

    @Test
    public void getRefunds(){
        Response response = refundsRequest.getRefunds(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsRefunds());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected the totalElements > 0", Integer.parseInt(
                refundsValidator.getTotalElements(jsonObject)) > 0);

        Assert.assertNotNull("It was expected the items value",refundsValidator.getItems(jsonObject));

        Assert.assertTrue("It was expected the amount value",refundsValidator.getAmount(jsonObject));

        Assert.assertTrue("It was expected the amount grater than 100",
                refundsValidator.getAmountGreaterThan100(jsonObject));

        Assert.assertTrue("It was expected the paymentId value",refundsValidator.getPaymentId(jsonObject));

        Assert.assertTrue("It was expected the creationDate value",
                refundsValidator.getCreationDate(jsonObject));

        Assert.assertTrue("It was expected the status value",refundsValidator.getStatus(jsonObject));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(refundsValidator.getCreationDateList(jsonObject)
                        .stream().findFirst().orElseThrow(()-> new NoSuchElementException("creation date are empty"))));
    }

    @Test
    public void getRefundById(){
        Response response = refundsRequest.getRefunds(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsRefund());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected a value greater than 0",
                Integer.parseInt(refundsValidator.getRefundAmount(jsonObject)) > 100);

        Assert.assertTrue("It was expected a getCapturePaymentId value",
                StringUtils.isNotEmpty(refundsValidator.getRefundPaymentId(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(refundsValidator.getRefundCreationDate(jsonObject)));

        Assert.assertTrue("It was expected a getCaptureStatus value",
                StringUtils.isNotEmpty(refundsValidator.getRefundStatus(jsonObject)));
    }
}