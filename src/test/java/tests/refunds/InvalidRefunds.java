package tests.refunds;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import refunds.RefundsRequest;
import token.GeneratedTokenProperties;

public class InvalidRefunds {

    private static RefundsRequest refundsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    @BeforeClass
    public static void initClass(){
        refundsRequest = new RefundsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Test
    public void getInvalidRefunds(){
        Response response = refundsRequest.getRefunds(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidTransactionsRefund());

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        Assert.assertEquals("It was expected an empty response", 0,
                response.body().prettyPrint().length());
    }
}
