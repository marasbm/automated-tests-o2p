package tests.elastic;

import balances.BalancesRequest;
import balances.BalancesValidator;
import breakdowns.BreakdownRequest;
import breakdowns.BreakdownValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import elastic.ElasticRequest;
import elastic.ElasticValidator;
import helpers.HelpersValidator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ValidElastic {

    private static ElasticRequest elasticRequest;

    private static BalancesRequest balancesRequest;

    private static BreakdownRequest breakdownRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BalancesValidator balancesValidator;

    private BreakdownValidator breakdownValidator;

    private ElasticValidator elasticValidator;

    @BeforeClass
    public static void initClass(){
        elasticRequest = new ElasticRequest();

        balancesRequest = new BalancesRequest();

        breakdownRequest = new BreakdownRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        elasticValidator = new ElasticValidator();

        balancesValidator = new BalancesValidator();

        breakdownValidator = new BreakdownValidator();
    }

    @Test
    public void getElasticCreatedBalance() throws InterruptedException {
        Response responseCreateBalance = balancesRequest.postBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                balancesRequest.balanceWithGeneratedIdAndValidValuesRequestBody());

        Assert.assertEquals("It was expected the status code 201",201,
                responseCreateBalance.statusCode());

        JSONArray jsonArray = HelpersValidator.getResponseBodyToJsonArray(responseCreateBalance);

        HelpersValidator.sleepFourSeconds();

        String balanceId = balancesValidator.getBalanceId(jsonArray);

        Response responseElastic = elasticRequest.getElasticBalancesRequest();

        JSONObject jsonObjectElastic = HelpersValidator.getResponseBodyToJsonObject(responseElastic);

        Assert.assertEquals("It was expected the same balanceId",
                elasticValidator.elasticGetBalanceId(jsonObjectElastic,balanceId), balanceId);

        Assert.assertEquals("It was expected the same amount",
                elasticValidator.elasticGetAmount(jsonObjectElastic,balanceId), balancesValidator.getAmount(jsonArray));

        Assert.assertEquals("It was expected the same sender",
                elasticValidator.elasticGetSender(jsonObjectElastic,balanceId), balancesValidator.getSender(jsonArray));

        Assert.assertEquals("It was expected the same receiver",
                elasticValidator.elasticGetReceiver(jsonObjectElastic,balanceId), balancesValidator.getReceiver(jsonArray));
    }

    @Test
    public void getElasticAllBalancesResults(){
        Response responseElastic = elasticRequest.getElasticBalancesRequest();

        JSONObject jsonObjectElastic = HelpersValidator.getResponseBodyToJsonObject(responseElastic);

        int elasticTotalHits = Integer.parseInt(elasticValidator.getHitsTotalValue(jsonObjectElastic));

        Assume.assumeTrue(Integer.parseInt(configurationsProperties.elasticBalancesLimitResults())> elasticTotalHits);

        Assert.assertEquals("It was expected the same", elasticTotalHits,
                elasticValidator.getHitsValueToJsonObject(jsonObjectElastic).size());
    }

    @Test
    public void getElasticCreatedBreakdown() throws InterruptedException {
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        HelpersValidator.sleepFourSeconds();

        String breakdownId = breakdownValidator.getBreakdownId(jsonObject);

        Response responseElastic = elasticRequest.getElasticBreakdownsRequest();

        JSONObject jsonObjectElastic = HelpersValidator.getResponseBodyToJsonObject(responseElastic);

        Assert.assertEquals("It was expected the same breakdownId",
                elasticValidator.elasticGetBreakdownId(jsonObjectElastic, breakdownId), breakdownId);

        Assert.assertEquals("It was expected the same transactionId",
                elasticValidator.elasticGetTransactionId(jsonObjectElastic, breakdownId),
                breakdownValidator.getTransactionId(jsonObject));

        Assert.assertTrue("It was expected the transfers object",
                elasticValidator.elasticGetTransfers(jsonObjectElastic, breakdownId).length() > 0);
    }

    @Test
    public void getElasticAllBreakdownsResults(){
        Response responseElastic = elasticRequest.getElasticBreakdownsRequest();

        JSONObject jsonObjectElastic = HelpersValidator.getResponseBodyToJsonObject(responseElastic);

        int elasticTotalHits = Integer.parseInt(elasticValidator.getHitsTotalValue(jsonObjectElastic));

        Assume.assumeTrue(Integer.parseInt(configurationsProperties.elasticBalancesLimitResults())> elasticTotalHits);

        Assert.assertEquals("It was expected the same", elasticTotalHits,
                elasticValidator.getHitsValueToJsonObject(jsonObjectElastic).size());
    }
}