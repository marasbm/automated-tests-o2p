package tests.payoutrules;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import payoutrules.PayoutRulesRequest;
import payoutrules.PayoutRulesValidator;
import token.GeneratedTokenProperties;

public class InvalidPayoutRules {

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private static PayoutRulesRequest payoutRulesRequest;

    private PayoutRulesValidator payoutRulesValidator;

    @BeforeClass
    public static void initClass(){
        payoutRulesRequest = new PayoutRulesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        payoutRulesValidator = new PayoutRulesValidator();
    }

    @Test
    public void getInvalidBeneficiaryRequestPayoutRulesFromBeneficiary(){
        Response response = payoutRulesRequest.getPayoutRulesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiariesBaseUri().concat("/")
                        .concat(configurationsProperties.invalidBeneficiary()
                                .concat(configurationsProperties.payoutRulesBaseUri())));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());
    }

    @Test
    public void getInvalidRequestPayoutRulesFromBeneficiary(){
        Response response = payoutRulesRequest.getPayoutRulesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiariesBaseUri().concat("/")
                        .concat(configurationsProperties.invalidBeneficiary()
                                .concat("/payoutRule")));

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the same error", "Not Found",
                payoutRulesValidator.getError(jsonObject));
    }
}
