package tests.payoutrules;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import payoutrules.PayoutRulesRequest;
import payoutrules.PayoutRulesValidator;
import token.GeneratedTokenProperties;

public class ValidPayoutRules {

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private static PayoutRulesRequest payoutRulesRequest;

    private PayoutRulesValidator payoutRulesValidator;

    @BeforeClass
    public static void initClass(){
        payoutRulesRequest = new PayoutRulesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        payoutRulesValidator = new PayoutRulesValidator();
    }

    @Test
    public void getPayoutRulesFromBeneficiary(){
        Response response = payoutRulesRequest.getPayoutRulesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiariesBaseUri().concat("/")
                        .concat(configurationsProperties.validBeneficiary()
                                .concat(configurationsProperties.payoutRulesBaseUri())));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected valid cronExpression",HelpersValidator.validateCronExpression(
                payoutRulesValidator.getCronExpression(jsonObject)));

        Assert.assertTrue("It was expected automaticAmount as boolean", !Boolean.parseBoolean(
                payoutRulesValidator.getAutomaticMode(jsonObject)) || Boolean.parseBoolean(
                payoutRulesValidator.getAutomaticMode(jsonObject)));

        Assert.assertTrue("It was expected reservedAmount as integer", Integer.parseInt(
                payoutRulesValidator.getReservedAmount(jsonObject)) > 0);

        Assert.assertTrue("It was expected transferHoldingDays as integer", Integer.parseInt(
                payoutRulesValidator.getTransfersHoldingDays(jsonObject)) >= 0);

        Assert.assertTrue("It was expected minimumAmount as integer", Integer.parseInt(
                payoutRulesValidator.getMinimumAmount(jsonObject)) > 0);
    }

    @Test
    public void getPayoutRulesFromMarketPlace(){
        Response response = payoutRulesRequest.getPayoutRulesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiariesBaseUri().concat("/")
                        .concat(configurationsProperties.validBeneficiaryWithoutPayoutRules()
                                .concat(configurationsProperties.payoutRulesBaseUri())));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected valid cronExpression",HelpersValidator.validateCronExpression(
                payoutRulesValidator.getCronExpression(jsonObject)));

        Assert.assertTrue("It was expected automaticAmount as boolean", !Boolean.parseBoolean(
                payoutRulesValidator.getAutomaticMode(jsonObject)) || Boolean.parseBoolean(
                payoutRulesValidator.getAutomaticMode(jsonObject)));

        Assert.assertTrue("It was expected reservedAmount as integer", Integer.parseInt(
                payoutRulesValidator.getReservedAmount(jsonObject)) > 0);

        Assert.assertTrue("It was expected transferHoldingDays as integer", Integer.parseInt(
                payoutRulesValidator.getTransfersHoldingDays(jsonObject)) >= 0);

        Assert.assertTrue("It was expected minimumAmount as integer", Integer.parseInt(
                payoutRulesValidator.getMinimumAmount(jsonObject)) > 0);
    }
}