package tests.security;

import com.jayway.restassured.response.Response;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.*;
import security.SecurityRequest;
import security.SecurityValidator;
import token.GeneratedToken;
import token.GeneratedTokenProperties;

import javax.xml.bind.DatatypeConverter;


public class Security {

    private static  SecurityRequest securityRequest;

    private static GeneratedToken generatedtoken;

    private SecurityValidator securityValidator;

    @BeforeClass
    public static void initClass(){
        securityRequest = new SecurityRequest();

        generatedtoken = new GeneratedToken();
    }

    @Before
    public void initTest(){
        securityValidator = new SecurityValidator();
    }

    @Test
    public void implementationOfTheAuthorizationServer() throws Exception {
        Response response = securityRequest.oauthPost();

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        String accessToken = securityValidator.getAccessToken(jsonObject);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue(StringUtils.isNotEmpty(accessToken));

        Assert.assertEquals("It was expected bearer", "bearer",
                securityValidator.getTokenType(jsonObject));

        generatedtoken.SaveGeneratedTokenInXml(securityValidator.getAccessToken(jsonObject));
    }

    @Test
    public void extractMarketPlaceIdFromToken(){

        GeneratedTokenProperties generatedTokenProperties = new GeneratedTokenProperties();

        String[] jwtTokenSplit = StringUtils.split(generatedTokenProperties.generatedToken(),".");

        Assume.assumeTrue("It was expected 3 positions",jwtTokenSplit.length == 3);

        String parsedBase64 = new String(DatatypeConverter.parseBase64Binary(jwtTokenSplit[1]));

        String marketPlaceId = StringUtils.split(parsedBase64,",")[3];

        Assert.assertEquals("It was expected client_id", "\"client_id\":\"wynd-client-sandbox",
                marketPlaceId);
    }
}