package tests.transactions;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;
import transactions.TransactionsRequest;
import transactions.TransactionsValidator;

public class InvalidTransactions {

    private static TransactionsRequest transactionsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private TransactionsValidator transactionsValidator;

    @BeforeClass
    public static void initClass(){
        transactionsRequest = new TransactionsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        transactionsValidator = new TransactionsValidator();
    }

    @Test
    public void getTransactionWithOrderId(){
        Response responseGetTransactions = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList());

        Assert.assertEquals("It was expected the status code 206", 206,
                responseGetTransactions.statusCode());

        JSONObject getTransactions = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactions);

        JSONObject firstTransaction = transactionsValidator.getFirstTransactionToJSonObject(getTransactions);

        String orderId = transactionsValidator.getOrderId(firstTransaction);

        Response responseGetTransactionsWithOrderId = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat("/").concat(orderId));

        Assert.assertEquals("It was expected the status code 404", 404,
                responseGetTransactionsWithOrderId.statusCode());
    }

    @Test
    public void getTransactionWithBeneficiary(){
        Response responseGetTransactionsWithOrderId = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat("/").concat(
                        configurationsProperties.validBeneficiary()));

        Assert.assertEquals("It was expected the status code 404", 404,
                responseGetTransactionsWithOrderId.statusCode());

        //TODO: Bug found
    }

    @Test
    public void getTransactionWithPaymentId(){
        Response responseGetTransactions = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList());

        Assert.assertEquals("It was expected the status code 206", 206,
                responseGetTransactions.statusCode());

        JSONObject getTransactions = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactions);

        JSONObject firstTransaction = transactionsValidator.getFirstTransactionToJSonObject(getTransactions);

        String transactionsCapturePaymentId = transactionsValidator.getTransactionCapturesPaymentId(firstTransaction);

        Response responseGetTransactionsWithOrderId = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat("/").concat(transactionsCapturePaymentId));

        Assert.assertEquals("It was expected the status code 404", 404,
                responseGetTransactionsWithOrderId.statusCode());
    }
}