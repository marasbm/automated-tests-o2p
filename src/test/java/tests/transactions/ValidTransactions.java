package tests.transactions;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;
import transactions.TransactionsRequest;
import transactions.TransactionsValidator;

public class ValidTransactions {

    private static TransactionsRequest transactionsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private TransactionsValidator transactionsValidator;

    @BeforeClass
    public static void initClass(){
        transactionsRequest = new TransactionsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        transactionsValidator = new TransactionsValidator();
    }

    @Test
    public void getAllTransactions(){
        Response responseGetTransactions = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList());

        Assert.assertEquals("It was expected the status code 206", 206,
                responseGetTransactions.statusCode());

        JSONObject getTransactions = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactions);

        Assert.assertTrue("It was expected an transactionsList with values",
                transactionsValidator.getTransactionsIds(getTransactions).size() > 1);
    }
}