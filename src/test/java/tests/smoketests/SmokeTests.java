package tests.smoketests;

import authorizations.AuthorizationsRequest;
import balances.BalancesRequest;
import beneficiariesandmarketplaceusers.BeneficiariesAndMarketplaceUsersRequest;
import beneficiariessmoney.BeneficiariesSmoneyRequest;
import breakdowns.BreakdownRequest;
import captures.CapturesRequest;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import paymentpageauthorization.PaymentPageAuthorizationRequest;
import payoutrules.PayoutRulesRequest;
import refunds.RefundsRequest;
import token.GeneratedTokenProperties;
import transactions.TransactionsRequest;


public class SmokeTests {

    private static TransactionsRequest transactionsRequest;

    private static AuthorizationsRequest authorizationsRequest;

    private static BalancesRequest balancesRequest;

    private static BeneficiariesSmoneyRequest beneficiariesSmoneyRequest;

    private static BeneficiariesAndMarketplaceUsersRequest beneficiariesAndMarketplaceUsersRequest;

    private static BreakdownRequest breakdownRequest;

    private static CapturesRequest capturesRequest;

    private static PaymentPageAuthorizationRequest paymentPageAuthorizationRequest;

    private static PayoutRulesRequest payoutRulesRequest;

    private static RefundsRequest refundsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    @BeforeClass
    public static void initClass(){
        transactionsRequest = new TransactionsRequest();

        authorizationsRequest = new AuthorizationsRequest();

        balancesRequest = new BalancesRequest();

        beneficiariesAndMarketplaceUsersRequest = new BeneficiariesAndMarketplaceUsersRequest();

        beneficiariesSmoneyRequest = new BeneficiariesSmoneyRequest();

        breakdownRequest = new BreakdownRequest();

        capturesRequest = new CapturesRequest();

        payoutRulesRequest = new PayoutRulesRequest();

        paymentPageAuthorizationRequest = new PaymentPageAuthorizationRequest();

        refundsRequest = new RefundsRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Test
    public void getAllTransactions(){
        Response responseGetTransactions = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList());

        Assert.assertEquals("It was expected the status code 206", 206,
                responseGetTransactions.statusCode());
    }

    @Test
    public void getAuthorizations(){
        Response response = authorizationsRequest.getAuthorizations(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsAuthorizations());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());
    }

    @Test
    public void getBalances() {
        Response responseGetBalances = balancesRequest.getBalancesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.balancesLimit100());

        Assert.assertEquals("It was expected the status code 206", 206, responseGetBalances.statusCode());
    }

    @Test
    public void getUsersFromBeneficiary(){
        Response response = beneficiariesAndMarketplaceUsersRequest.getUsersFromBeneficiary(
                configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.validBeneficiary());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());
    }

    @Test
    public void getBeneficiarySmoney(){
        Response responseGetBeneficiarySmoney = beneficiariesSmoneyRequest.getBeneficiariesSmoney();

        Assert.assertEquals("It was expected the status code 200",200,
                responseGetBeneficiarySmoney.statusCode());
    }

    @Test
    public void getBreakdowns(){
        Response response = breakdownRequest.getBreakdownsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.breakdownsBaseUri());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());
    }

    @Test
    public void getCaptures(){
        Response response = capturesRequest.getCaptures(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsCaptures());

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());
    }

    @Test
    public void securityPaymentPageUrl(){
        Response response = paymentPageAuthorizationRequest.paymentPageAuthorization(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                paymentPageAuthorizationRequest.jsonObjectPaymentPageAuthorizationCompleteRequestBody());

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());
    }

    @Test
    public void getPayoutRulesFromBeneficiary(){
        Response response = payoutRulesRequest.getPayoutRulesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.beneficiariesBaseUri().concat("/")
                        .concat(configurationsProperties.validBeneficiary()
                                .concat(configurationsProperties.payoutRulesBaseUri())));

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());
    }

    @Test
    public void getRefunds(){
        Response response = refundsRequest.getRefunds(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsRefunds());

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());
    }
}
