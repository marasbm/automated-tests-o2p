package tests.captures;

import captures.CapturesRequest;
import captures.CapturesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

import java.time.LocalDateTime;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvalidCaptures {

    private static CapturesRequest capturesRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private CapturesValidator capturesValidator;

    @BeforeClass
    public static void initClass(){
        capturesRequest = new CapturesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        capturesValidator = new CapturesValidator();
    }

    @Test
    public void getInvalidCaptures(){
        Response response = capturesRequest.getCaptures(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.invalidTransactionsCaptures());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 404",404, response.statusCode());

        Assert.assertEquals("It was expected the error Not Found",
                "Not Found",capturesValidator.getError(jsonObject));
    }

    @Test
    public void capturesWithNotActiveBeneficiary(){
        String paymentId = "PAYMENT_ID_".concat(LocalDateTime.now().toString());

        String breakdownId = "BREAKDOWN_ID_".concat(LocalDateTime.now().toString());

        String transferId1 = "TRANSFER_ID_1_".concat(LocalDateTime.now().toString());

        String transferId2 = "TRANSFER_ID_2_".concat(LocalDateTime.now().toString());

        Response response = capturesRequest.captureRequest(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),
                capturesRequest.capturesWithBreakdownsAndTransfersAndNotValidBeneficiaryRequestBody(
                        paymentId,breakdownId,transferId1,transferId2,configurationsProperties.beneficiaryNotActive()));

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                capturesValidator.getError(jsonObject));

        Assert.assertTrue("It was expected Beneficiary not valid",capturesValidator.getMessage(jsonObject)
                .endsWith("Beneficiary not valid"));
    }
}