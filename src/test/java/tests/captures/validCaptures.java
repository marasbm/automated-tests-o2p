package tests.captures;

import captures.CapturesRequest;
import captures.CapturesValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import token.GeneratedTokenProperties;

import java.util.NoSuchElementException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class validCaptures {

    private static CapturesRequest capturesRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private CapturesValidator capturesValidator;

    @BeforeClass
    public static void initClass(){
        capturesRequest = new CapturesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        capturesValidator = new CapturesValidator();
    }

    @Test
    public void getCaptures(){
        Response response = capturesRequest.getCaptures(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsCaptures());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        Assert.assertTrue("It was expected the totalElements > 0", Integer.parseInt(
                capturesValidator.getTotalElements(jsonObject)) > 0);

        Assert.assertNotNull("It was expected the items value",capturesValidator.getItems(jsonObject));

        Assert.assertTrue("It was expected the amount value",capturesValidator.getAmount(jsonObject));

        Assert.assertTrue("It was expected the paymentId value",capturesValidator.getPaymentId(jsonObject));

        Assert.assertTrue("It was expected the authorizationId value",
                capturesValidator.getAuthorizationId(jsonObject));

        Assert.assertTrue("It was expected the creationDate value",
                capturesValidator.getCreationDate(jsonObject));

        Assert.assertTrue("It was expected the status value",capturesValidator.getStatus(jsonObject));

        Assert.assertTrue("It was expected the breakdown value",capturesValidator.getBreakdown(jsonObject));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(capturesValidator.getCreationDateList(jsonObject)
                        .stream().findFirst().orElseThrow(()-> new NoSuchElementException("creation date are empty"))));
    }

    @Test
    public void getCaptureById(){
        Response response = capturesRequest.getCaptures(configurationsProperties.authorization(),
                generatedTokenProperties.generatedToken(),configurationsProperties.transactionsCapture());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertTrue("It was expected a value greater than 0",
                Integer.parseInt(capturesValidator.getCaptureAmount(jsonObject)) > 100);

        Assert.assertTrue("It was expected a getCapturePaymentId value",
                StringUtils.isNotEmpty(capturesValidator.getCapturePaymentId(jsonObject)));

        Assert.assertTrue("It was expected a getCapturePaymentId value",
                StringUtils.isNotEmpty(capturesValidator.getCaptureAuthorizationId(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(capturesValidator.getCaptureCreationDate(jsonObject)));

        Assert.assertTrue("It was expected a getCaptureStatus value",
                StringUtils.isNotEmpty(capturesValidator.getCaptureStatus(jsonObject)));

        Assert.assertTrue("It was expected a getCaptureBreakdownId value",
                StringUtils.isNotEmpty(capturesValidator.getCaptureBreakdownId(jsonObject)));

        Assert.assertEquals("It was expected the same amount",
                Integer.parseInt(capturesValidator.getCaptureAmount(jsonObject)),
                capturesValidator.transfersTotalAmount(jsonObject));

        Assert.assertTrue("It was expected the subOrder value",
                capturesValidator.validateTransfersSubOrderIdNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the beneficiary value",
                capturesValidator.validateBeneficiaryNotEmpty(jsonObject));

        Assert.assertTrue("It was expected all transfersId with different values",
                capturesValidator.validateAllTransferIdNotEquals(jsonObject));
    }
}