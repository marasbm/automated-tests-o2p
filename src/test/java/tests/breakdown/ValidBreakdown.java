package tests.breakdown;

import beneficiaries.BeneficiariesRequest;
import breakdowns.BreakdownRequest;
import breakdowns.BreakdownValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;
import transactions.TransactionsRequest;
import transactions.TransactionsValidator;

import java.util.NoSuchElementException;

public class ValidBreakdown {

    private static BreakdownRequest breakdownRequest;

    private static TransactionsRequest transactionsRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private static BeneficiariesRequest beneficiariesRequest;

    private BreakdownValidator breakdownValidator;

    private TransactionsValidator transactionsValidator;

    @BeforeClass
    public static void initClass(){
        breakdownRequest = new BreakdownRequest();

        transactionsRequest = new TransactionsRequest();

        beneficiariesRequest = new BeneficiariesRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        breakdownValidator = new BreakdownValidator();

        transactionsValidator = new TransactionsValidator();
    }

    @Test
    public void createBreakdown(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBody());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 201",201, response.statusCode());

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(breakdownValidator.getCreationDate(jsonObject)));

        Assert.assertTrue("It was expected the getBreakdownId with value",
                StringUtils.isNotEmpty(breakdownValidator.getBreakdownId(jsonObject)));

        Assert.assertTrue("It was expected the getTransactionId with value",
                StringUtils.isNotEmpty(breakdownValidator.getTransactionId(jsonObject)));

        Assert.assertTrue("It was expected the TransfersSubOrderId with value",
                breakdownValidator.validateTransfersSubOrderIdNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the all TransfersId different",
                breakdownValidator.validateAllTransferIdNotEquals(jsonObject));

        Assert.assertTrue("It was expected the all Transfer amount > 100",
                breakdownValidator.validateAllTransfersAmountGreaterThanZero(jsonObject));

        Assert.assertTrue("It was expected the Beneficiary with value",
                breakdownValidator.validateTransfersBeneficiaryNotEmpty(jsonObject));

        Response responseGetTransactionsById = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat(
                        "/"+breakdownValidator.getTransactionId(jsonObject)+""));

        JSONObject TransactionsByIdJsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactionsById);

        Assert.assertEquals("It was expected the same TransactionId",
                breakdownValidator.getTransactionId(jsonObject), transactionsValidator.getTransactionId(
                        TransactionsByIdJsonObject));
    }

    @Test
    public void getBreakdowns(){
        Response response = breakdownRequest.getBreakdownsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.breakdownsBaseUri());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 206",206, response.statusCode());

        Assert.assertTrue("It was expected the totalElements > 0", Integer.parseInt(
                breakdownValidator.getTotalElements(jsonObject)) > 0);

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(breakdownValidator.getItemsCreationDateList(jsonObject)
                        .stream().findFirst().orElseThrow(()-> new NoSuchElementException("creation date are empty"))));

        Assert.assertTrue("validate items BreakdownIdNotEmpty",
                breakdownValidator.validateItemsBreakdownIdNotEmpty(jsonObject));

        Assert.assertTrue("validate items BreakdownIdNotEmpty",
                breakdownValidator.validateItemsTransactionIdNotEmpty(jsonObject));

        Assert.assertTrue("validate items status NotEmpty",
                breakdownValidator.validateItemsStatusIdNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the transfer amount > 0",
                Integer.parseInt(breakdownValidator.itemsTransfersAmount(jsonObject)) > 0);

        Assert.assertTrue("It was expected a value",
                StringUtils.isNotEmpty(breakdownValidator.itemsTransfersSubOrderId(jsonObject)));

        Assert.assertTrue("It was expected a value",
                StringUtils.isNotEmpty(breakdownValidator.itemsTransfersBeneficiary(jsonObject)));

        Assert.assertTrue("It was expected a value",
                StringUtils.isNotEmpty(breakdownValidator.itemsTransfersTransferId(jsonObject)));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(breakdownValidator.itemsTransfersCreationDate(jsonObject)));
    }

    @Test
    public void getBreakdownById(){
        Response response = breakdownRequest.getBreakdownsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.breakdownsBaseUri().concat("/").concat(configurationsProperties.breakdownById()));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 200",200, response.statusCode());

        Assert.assertEquals("It was expected the same",
                configurationsProperties.breakdownById(), breakdownValidator.getBreakdownId(jsonObject));

        Assert.assertTrue("It was expected the TransfersSubOrderId with value",
                breakdownValidator.validateTransfersSubOrderIdNotEmpty(jsonObject));

        Assert.assertTrue("It was expected the all TransfersId different",
                breakdownValidator.validateAllTransferIdNotEquals(jsonObject));

        Assert.assertTrue("It was expected the all Transfer amount > 0",
                breakdownValidator.validateAllTransfersAmountGreaterThanZero(jsonObject));

        Assert.assertTrue("It was expected the Beneficiary with value",
                breakdownValidator.validateTransfersBeneficiaryNotEmpty(jsonObject));

        Response responseGetTransactionsById = transactionsRequest.getTransactionsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat(
                        "/"+breakdownValidator.getTransactionId(jsonObject)));

        JSONObject transactionsByIdJsonObject = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactionsById);

        Assert.assertEquals("It was expected the same", breakdownValidator.getTransactionId(jsonObject),
                transactionsValidator.getTransactionId(transactionsByIdJsonObject));

        Assert.assertTrue("It was expected the parse dateTime value",
                HelpersValidator.validateDateFormatWithMills(breakdownValidator.getCreationDate(jsonObject)));

        Response responseGetBeneficiaryById = beneficiariesRequest.getBeneficiariesRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.transactionsList().concat(
                        "/"+breakdownValidator.getTransfersBeneficiaryId(jsonObject)));

        JSONObject beneficiaryById = HelpersValidator.getResponseBodyToJsonObject(responseGetTransactionsById);

        //TODO: Get beneficiaties

        //TODO: Continue this test after solve bug beneficiaries

        //TODO: Validate the status after Martin specification
    }
}