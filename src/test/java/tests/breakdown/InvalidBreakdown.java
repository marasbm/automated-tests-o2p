package tests.breakdown;

import breakdowns.BreakdownRequest;
import breakdowns.BreakdownValidator;
import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import token.GeneratedTokenProperties;

public class InvalidBreakdown {

    private static BreakdownRequest breakdownRequest;

    private static ConfigurationsProperties configurationsProperties;

    private static GeneratedTokenProperties generatedTokenProperties;

    private BreakdownValidator breakdownValidator;

    @BeforeClass
    public static void initClass(){
        breakdownRequest = new BreakdownRequest();

        configurationsProperties = new ConfigurationsProperties();

        generatedTokenProperties = new GeneratedTokenProperties();
    }

    @Before
    public void initTest(){
        breakdownValidator = new BreakdownValidator();
    }

    @Test
    public void createBreakdownWithNoRequiredFields(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyWithNoRequiredFields());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 3",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNoAmountRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyWithNoRequiredFieldAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNullAmountRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyNullRequiredFieldAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNotValidAmountRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyNotValidRequiredFieldAmount());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNotValidTransactionIdRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyNotValidRequiredFieldTransactionId());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Transaction not found",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNullTransactionIdRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyWithNoRequiredFieldTransactionId());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void createBreakdownWithNotValidBeneficiaryRequiredField(){
        Response response = breakdownRequest.postBreakdownRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                breakdownRequest.postBreakdownRequestBodyNotValidRequiredFieldBeneficiary());

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assert.assertEquals("It was expected the status code 400",400, response.statusCode());

        Assert.assertEquals("It was expected Bad Request","Bad Request",
                breakdownValidator.getError(jsonObject));

        Assert.assertEquals("It was expected Validation failed",
                "Validation failed for object='breakdownRequest'. Error count: 1",
                breakdownValidator.getMessage(jsonObject));
    }

    @Test
    public void getBreakdownWithTransactionId(){
        Response response = breakdownRequest.getBreakdownsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.breakdownsBaseUri().concat("/").concat(configurationsProperties.breakdownById()));

        JSONObject jsonObject = HelpersValidator.getResponseBodyToJsonObject(response);

        String transactionId = breakdownValidator.getTransactionId(jsonObject);

        Response responseWithInvalidGet = breakdownRequest.getBreakdownsRequest(
                configurationsProperties.authorization(), generatedTokenProperties.generatedToken(),
                configurationsProperties.breakdownsBaseUri().concat("/").concat(transactionId));

        Assert.assertEquals("It was expected the status code 404",404,
                responseWithInvalidGet.statusCode());

        //TODO: Bug found Get Breakdown With TransactionId FN4OC – 539
    }
}