package paymentpagecapture;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;
import static com.jayway.restassured.RestAssured.given;

public class PaymentPageCaptureRequest {

    private ConfigurationsProperties configurationsProperties;

    public PaymentPageCaptureRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response paymentPageCaptureRequest(String authorizationKey, String authorizationValue, String requestBody) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.paymentPages());
    }

    public String paymentPageCaptureRequestBody(String beneficiary) {
        return "{\n" +
                    "\"orderId\":\"payment-order\",\n" +
                    "\"amount\":10000,\n" +
                    "\"currency\":\"EUR\",\n" +
                    "\"returnUrl\":\"https://your_domain.com/return\",\n" +
                    "\"responseUrl\":\"https://your_domain.com/confirm\",\n" +
                    "\"usePaymentMeanToken\":true,\n" +
                    "\"customerId\":\"user-1234\",\n" +
                    "\"paymentPageType\":\"\",\n" +
                    "\"JSON_addData\":null,\n" +
                    "\"sourceChannel\":\"WEB\",\n" +
                    "\"capture\":{\n" +
                    "\"amount\":\"10000\",\n" +
                    "\"breakdown\":{\n" +
                    "\"transfers\":[\n" +
                    "{\n" +
                    "\"amount\":\"5000\",\n" +
                    "\"subOrderId\":\"123\",\n" +
                    "\"beneficiary\":\""+beneficiary+"\"\n" +
                    "},\n" +
                    "{\n" +
                    "\"amount\":\"5000\",\n" +
                    "\"subOrderId\":\"123\",\n" +
                    "\"beneficiary\":\""+beneficiary+"\"\n" +
                    "}\n" +
                    "],\n" +
                    "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                    "}\n" +
                    "}\n" +
                    "}";
    }

    public String paymentPageCaptureRequestBodyNoRequiredFieldAmount() {
        return "{\n" +
                "\"orderId\":\"payment-order\",\n" +
                "\"currency\":\"EUR\",\n" +
                "\"returnUrl\":\"https://your_domain.com/return\",\n" +
                "\"responseUrl\":\"https://your_domain.com/confirm\",\n" +
                "\"usePaymentMeanToken\":true,\n" +
                "\"customerId\":\"user-1234\",\n" +
                "\"paymentPageType\":\"\",\n" +
                "\"JSON_addData\":null,\n" +
                "\"sourceChannel\":\"WEB\",\n" +
                "\"capture\":{\n" +
                "\"amount\":\"10000\",\n" +
                "\"breakdown\":{\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"5000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}\n" +
                "}\n" +
                "}";
    }

    public String paymentPageCaptureRequestBodyCaptureGratedThanAmount() {
        return "{\n" +
                "\"orderId\":\"payment-order\",\n" +
                "\"amount\":10000,\n" +
                "\"currency\":\"EUR\",\n" +
                "\"returnUrl\":\"https://your_domain.com/return\",\n" +
                "\"responseUrl\":\"https://your_domain.com/confirm\",\n" +
                "\"usePaymentMeanToken\":true,\n" +
                "\"customerId\":\"user-1234\",\n" +
                "\"paymentPageType\":\"\",\n" +
                "\"JSON_addData\":null,\n" +
                "\"sourceChannel\":\"WEB\",\n" +
                "\"capture\":{\n" +
                "\"amount\":\"10000\",\n" +
                "\"breakdown\":{\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"15000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}\n" +
                "}\n" +
                "}";
    }

    public String paymentPageCaptureRequestBodyCaptureMinorThanAmount() {
        return "{\n" +
                "\"orderId\":\"payment-order\",\n" +
                "\"amount\":10000,\n" +
                "\"currency\":\"EUR\",\n" +
                "\"returnUrl\":\"https://your_domain.com/return\",\n" +
                "\"responseUrl\":\"https://your_domain.com/confirm\",\n" +
                "\"usePaymentMeanToken\":true,\n" +
                "\"customerId\":\"user-1234\",\n" +
                "\"paymentPageType\":\"\",\n" +
                "\"JSON_addData\":null,\n" +
                "\"sourceChannel\":\"WEB\",\n" +
                "\"capture\":{\n" +
                "\"amount\":\"10000\",\n" +
                "\"breakdown\":{\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}\n" +
                "}\n" +
                "}";
    }
}