package payoutrules;

import configurations.ConfigurationsProperties;
import org.json.JSONObject;

public class PayoutRulesValidator {

    private static ConfigurationsProperties configurationsProperties;

    public PayoutRulesValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public String getCronExpression(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.cronExpression()).toString();
    }

    public String getReservedAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.reservedAmount()).toString();
    }

    public String getTransfersHoldingDays(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.transfersHoldingDays()).toString();
    }

    public String getAutomaticMode(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.automaticMode()).toString();
    }

    public String getMinimumAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.minimumAmount()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }
}