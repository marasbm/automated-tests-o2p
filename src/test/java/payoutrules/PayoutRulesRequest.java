package payoutrules;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class PayoutRulesRequest {

    private ConfigurationsProperties configurationsProperties;

    public PayoutRulesRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getPayoutRulesRequest(String authorizationKey, String authorizationValue, String queryString) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.internalIntegrationEnvironment().concat(queryString));
    }
}