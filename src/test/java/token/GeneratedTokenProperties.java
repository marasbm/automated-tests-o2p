package token;

public class GeneratedTokenProperties {

    private GeneratedToken generatedToken;

    public GeneratedTokenProperties(){

        generatedToken = new GeneratedToken();
    }

    public String generatedToken(){
        return generatedToken.getGeneratedToken();
    }
}
