package token;

import org.junit.Assume;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Optional;

public class GeneratedToken {

    private Document parsedDocument;

    private String getTokenFolderFullPath() throws IOException {

        return new File(".").getCanonicalPath() + "/token";
    }

    public void SaveGeneratedTokenInXml(String generatedToken) throws Exception {
        try {
            File file = new File(getTokenFolderFullPath()+ "/acesstoken.xml");

            if(file.exists()){
                final boolean deletedFile = file.delete();

                Assume.assumeTrue(deletedFile);
            }

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("configuration");

            doc.appendChild(rootElement);

            Element browser = doc.createElement("generatedtoken");

            browser.appendChild(doc.createTextNode( "Bearer " + generatedToken));

            rootElement.appendChild(browser);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();

            Transformer transformer = transformerFactory.newTransformer();

            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(new File(getTokenFolderFullPath()+ "/accesstoken.xml"));

            transformer.transform(source, result);

        }catch (Exception exception) {

            throw new Exception("Cannot create the token xml file");
        }
    }

    String getGeneratedToken() {
        try {

            if(parsedDocument == null){

                Optional<String> path = Optional.of(getTokenFolderFullPath()+ "/accesstoken.xml");

                File file = new File(path.orElseThrow(()->new NoSuchElementException("GeneratedTokenFullPath")));

                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                parsedDocument = documentBuilder.parse(file);
            }

            return parsedDocument.getElementsByTagName("generatedtoken").item(0).getTextContent();

        } catch (Exception exception) {

            return null;
        }
    }
}