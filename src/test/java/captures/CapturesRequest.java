package captures;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class CapturesRequest {

    private ConfigurationsProperties configurationsProperties;

    public CapturesRequest(){

        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getCaptures(String authorizationKey, String authorizationValue, String queryString){
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }

    public Response captureRequest(String authKey, String authValue, String requestBody){
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.transactionsCaptures());
    }

    public String capturesWithBreakdownsAndTransfersAndNotValidBeneficiaryRequestBody(
            String paymentId, String breakdownId, String transferId1, String transferId2, String beneficiaryId){
        return "{\n" +
                "\"paymentId\":\""+paymentId+"\",\n" +
                "\"amount\":80000,\n" +
                "\"breakdown\":{\n" +
                "\"breakdownId\":\""+breakdownId+"\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"transferId\":\""+transferId1+"\",\n" +
                "\"amount\":\"30000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\""+beneficiaryId+"\"\n" +
                "},\n" +
                "{\n" +
                "\"transferId\":\""+transferId2+"\",\n" +
                "\"amount\":\"50000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"654321\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}\n" +
                "}";
    }
}