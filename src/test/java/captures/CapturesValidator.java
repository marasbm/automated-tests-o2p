package captures;

import configurations.ConfigurationsProperties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CapturesValidator {

    private static ConfigurationsProperties configurationsProperties;

    public CapturesValidator() {

        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public String getTotalElements(JSONObject jsonObj) {
        return getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString();
    }

    public JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getItems(jsonObj).length(); i++) {
            JSONObject object = getItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public boolean getAmount(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.amount()).toString()));
    }

    public boolean getPaymentId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.paymentId()).toString()));
    }

    public boolean getAuthorizationId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.authorizationId()).toString()));
    }

    public boolean getCreationDate(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.creationDate()).toString()));
    }

    public List<String> getCreationDateList(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.creationDate()
        ).toString()).collect(Collectors.toList());
    }

    public boolean getStatus(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.status()).toString()));
    }

    private List<String> itemsBreakdown(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.breakdown()
        ).toString()).collect(Collectors.toList());
    }

    public boolean getBreakdown(JSONObject jsonObj) {
        return itemsBreakdown(jsonObj).stream().allMatch(StringUtils::isNotEmpty);
    }

    public String getCaptureAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getCapturePaymentId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.paymentId()).toString();
    }

    public String getCaptureAuthorizationId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.authorizationId()).toString();
    }

    public String getCaptureCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getCaptureStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    private JSONObject getCaptureBreakdown(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.breakdown());
    }

    public String getCaptureBreakdownId(JSONObject jsonObj) {
        return getCaptureBreakdown(jsonObj).get(configurationsProperties.breakdownId()).toString();
    }

    public String getCaptureBreakdownTransactionId(JSONObject jsonObj) {
        return getCaptureBreakdown(jsonObj).get(configurationsProperties.transactionId()).toString();
    }

    private JSONArray getCaptureTransfers(JSONObject jsonObj) {
        return (JSONArray) getCaptureBreakdown(jsonObj).get(configurationsProperties.transfers());
    }

    private List<JSONObject> getCaptureTransfersToJSonObject(JSONObject jsonObj) {
        List<JSONObject> transfersList = new ArrayList<>();

        for (int i = 0; i < getCaptureTransfers(jsonObj).length(); i++) {
            JSONObject object = getCaptureTransfers(jsonObj).getJSONObject(i);
            transfersList.add(object);
        }

        return transfersList;
    }

    private List<String> transfersAmount(JSONObject jsonObj){
        return getCaptureTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.amount()
        ).toString()).collect(Collectors.toList());
    }

    private List<String> transfersTransferId(JSONObject jsonObj){
        return getCaptureTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.transferId()
        ).toString()).collect(Collectors.toList());
    }

    public int transfersTotalAmount(JSONObject jsonObj){
        int result =0;
        for (int i = 0; i < transfersAmount(jsonObj).size(); i++) {
            result += Integer.parseInt(transfersAmount(jsonObj).get(i));
        }

        return result;
    }

    public boolean validateTransfersSubOrderIdNotEmpty(JSONObject jsonObj){
        return getCaptureTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.subOrderId()).toString()));
    }

    public boolean validateBeneficiaryNotEmpty(JSONObject jsonObj){
        return getCaptureTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.beneficiary()).toString()));
    }

    public boolean validateAllTransferIdNotEquals(JSONObject jsonObj){
        return transfersTransferId(jsonObj).size() ==  transfersTransferId(jsonObj).stream().distinct().count();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }
}