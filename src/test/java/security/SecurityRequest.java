package security;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class SecurityRequest {

    private ConfigurationsProperties configurationsProperties;

    public SecurityRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response oauthPost() {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(configurationsProperties.authorization(), configurationsProperties.authorizationBasic());

        httpRequest.contentType(ContentType.URLENC);

        httpRequest.formParam(configurationsProperties.grantType(), configurationsProperties.clientCredentials());

        return httpRequest.post(configurationsProperties.oauthToken());
    }
}
