package security;

import configurations.ConfigurationsProperties;
import org.json.JSONObject;

public class SecurityValidator {

    private static ConfigurationsProperties configurationsProperties;

    public SecurityValidator(){

        configurationsProperties = new ConfigurationsProperties();
    }


    public String getAccessToken(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.accessToken()).toString();
    }

    public String getTokenType(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.tokenType()).toString();
    }
}
