package configurations;

public class ConfigurationsProperties {

    private final Configuration configuration;

    private final ConfigurationsConstants configurationsConstants;

    public ConfigurationsProperties(){

        configuration = new Configuration();

        configurationsConstants = new ConfigurationsConstants();
    }

    public String errorDescription(){
        return configuration.getConfigurationNode(configurationsConstants.ERROR_DESCRIPTION);
    }

    public String error(){
        return configuration.getConfigurationNode(configurationsConstants.ERROR);
    }

    public String authorization(){
        return configuration.getConfigurationNode(configurationsConstants.AUTHORIZATION);
    }

    public String authorizationBasic(){
        return configuration.getConfigurationNode(configurationsConstants.AUTHORIZATION_BASIC);
    }

    public String grantType(){
        return configuration.getConfigurationNode(configurationsConstants.GRANT_TYPE);
    }

    public String clientCredentials(){
        return configuration.getConfigurationNode(configurationsConstants.CLIENT_CREDENTIALS);
    }

    public String invalidAuthorization(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_AUTHORIZATION);
    }

    public String accessToken(){
        return configuration.getConfigurationNode(configurationsConstants.ACCESS_TOKEN);
    }

    public String tokenType(){
        return configuration.getConfigurationNode(configurationsConstants.TOKEN_TYPE);
    }

    public String oauthToken(){
        return configuration.getConfigurationNode(configurationsConstants.OAUTH_TOKEN);
    }

    public String inValidToken(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_TOKEN);
    }

    public String paymentPages(){
        return configuration.getConfigurationNode(configurationsConstants.PAYMENT_PAGES);
    }

    public String amount(){
        return configuration.getConfigurationNode(configurationsConstants.AMOUNT);
    }

    public String orderId(){
        return configuration.getConfigurationNode(configurationsConstants.ORDER_ID);
    }

    public String marketPlaceId(){
        return configuration.getConfigurationNode(configurationsConstants.MARKET_PLACE_ID);
    }

    public String type(){
        return configuration.getConfigurationNode(configurationsConstants.TYPE);
    }

    public String transactionId(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTION_ID);
    }

    public String responseUrl(){
        return configuration.getConfigurationNode(configurationsConstants.RESPONSE_URL);
    }

    public String customerId(){
        return configuration.getConfigurationNode(configurationsConstants.CUSTOMER_ID);
    }

    public String currency(){
        return configuration.getConfigurationNode(configurationsConstants.CURRENCY);
    }

    public String paymentPageUrl(){
        return configuration.getConfigurationNode(configurationsConstants.PAYMENT_PAGE_URL);
    }

    public String returnUrl(){
        return configuration.getConfigurationNode(configurationsConstants.RETURN_URL);
    }

    public String paymentMethod(){
        return configuration.getConfigurationNode(configurationsConstants.PAYMENT_METHOD);
    }

    public String sourceChannel(){
        return configuration.getConfigurationNode(configurationsConstants.SOURCE_CHANNEL);
    }

    public String capture(){
        return configuration.getConfigurationNode(configurationsConstants.CAPTURE);
    }

    public String paymentId(){
        return configuration.getConfigurationNode(configurationsConstants.PAYMENT_ID);
    }

    public String breakdown(){
        return configuration.getConfigurationNode(configurationsConstants.BREAKDOWN);
    }

    public String breakdownId(){
        return configuration.getConfigurationNode(configurationsConstants.BREAKDOWN_ID);
    }

    public String transfers(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSFERS);
    }

    public String subOrderId(){
        return configuration.getConfigurationNode(configurationsConstants.SUB_ORDER_ID);
    }

    public String beneficiary(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARY);
    }

    public String transferId(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSFER_ID);
    }

    public String message(){
        return configuration.getConfigurationNode(configurationsConstants.MESSAGE);
    }

    public String transactionsCaptures(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_CAPTURES);
    }

    public String pages(){
        return configuration.getConfigurationNode(configurationsConstants.PAGES);
    }

    public String pagesTotalElements(){
        return configuration.getConfigurationNode(configurationsConstants.PAGES_TOTAL_ELEMENTS);
    }

    public String items(){
        return configuration.getConfigurationNode(configurationsConstants.ITEMS);
    }

    public String authorizationId(){
        return configuration.getConfigurationNode(configurationsConstants.AUTHORIZATION_ID);
    }

    public String creationDate(){
        return configuration.getConfigurationNode(configurationsConstants.CREATION_DATE);
    }

    public String status(){
        return configuration.getConfigurationNode(configurationsConstants.STATUS);
    }

    public String transactionsCapture(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_CAPTURE);
    }

    public String transactionsAuthorizations(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_AUTHORIZATIONS);
    }

    public String transactionsAuthorization(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_AUTHORIZATION);
    }

    public String invalidTransactionsAuthorization(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_TRANSACTIONS_AUTHORIZATIONS);
    }

    public String invalidTransactionsCaptures(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_TRANSACTIONS_CAPTURES);
    }

    public String transactionsRefunds(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_REFUNDS);
    }

    public String transactionsRefund(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_REFUND);
    }

    public String invalidTransactionsRefund(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_TRANSACTIONS_REFUND);
    }

    public String receiver(){
        return configuration.getConfigurationNode(configurationsConstants.RECEIVER);
    }

    public String sender(){
        return configuration.getConfigurationNode(configurationsConstants.SENDER);
    }

    public String echoedMessage(){
        return configuration.getConfigurationNode(configurationsConstants.ECHOED_MESSAGE);
    }

    public String balanceId(){
        return configuration.getConfigurationNode(configurationsConstants.BALANCE_ID);
    }

    public String balancesLimit100(){
        return configuration.getConfigurationNode(configurationsConstants.BALANCES_LIMIT_100);
    }

    public String transactionsList(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSACTIONS_LIST);
    }

    public String balancesById(){
        return configuration.getConfigurationNode(configurationsConstants.BALANCES_BY_ID);
    }

    public String integrationEnvironment(){
        return configuration.getConfigurationNode(configurationsConstants.INTEGRATION_ENVIRONMENT);
    }

    public String balancesBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.BALANCES_BASE_URI);
    }

    public String breakdownsBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.BREAKDOWNS_BASE_URI);
    }

    public String beneficiaryNotActive(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARY_NOT_ACTIVE);
    }

    public String breakdownById(){
        return configuration.getConfigurationNode(configurationsConstants.BREAKDOWN_BY_ID);
    }

    public String beneficiariesBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARIES_BASE_URI);
    }

    public String beneficiaryId(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARY_ID);
    }

    public String payoutRulesBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.PAYOUT_RULES_BASE_URI);
    }

    public String validBeneficiary(){
        return configuration.getConfigurationNode(configurationsConstants.VALID_BENEFICIARY);
    }

    public String cronExpression(){
        return configuration.getConfigurationNode(configurationsConstants.CRON_EXPRESSION);
    }

    public String reservedAmount(){
        return configuration.getConfigurationNode(configurationsConstants.RESERVED_AMOUNT);
    }

    public String transfersHoldingDays(){
        return configuration.getConfigurationNode(configurationsConstants.TRANSFERS_HOLDING_DAYS);
    }

    public String automaticMode(){
        return configuration.getConfigurationNode(configurationsConstants.AUTOMATIC_MODE);
    }

    public String minimumAmount(){
        return configuration.getConfigurationNode(configurationsConstants.MINIMUM_AMOUNT);
    }

    public String invalidBeneficiary(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_BENEFICIARY);
    }

    public String validBeneficiaryWithoutPayoutRules(){
        return configuration.getConfigurationNode(configurationsConstants.VALID_BENEFICIARY_WITHOUT_PAYOUT_RULES);
    }

    public String description(){
        return configuration.getConfigurationNode(configurationsConstants.DESCRIPTION);
    }

    public String accountAmountBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.ACCOUNT_AMOUNT_BASE_URI);
    }

    public String beneficiaryToUpdate(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARY_TO_UPDATE);
    }

    public String beneficiariesBackOfficeUpdateStatus(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARIES_BACK_OFFICE_UPDATE_STATUS);
    }

    public String activated(){
        return configuration.getConfigurationNode(configurationsConstants.ACTIVATED);
    }

    public String notActivated(){
        return configuration.getConfigurationNode(configurationsConstants.NOT_ACTIVATED);
    }

    public String deleted(){
        return configuration.getConfigurationNode(configurationsConstants.DELETED);
    }

    public String statusCode(){
        return configuration.getConfigurationNode(configurationsConstants.STATUS_CODE);
    }

    public String email(){
        return configuration.getConfigurationNode(configurationsConstants.EMAIL);
    }

    public String legalName(){
        return configuration.getConfigurationNode(configurationsConstants.LEGAL_NAME);
    }

    public String notValid(){
        return configuration.getConfigurationNode(configurationsConstants.NOT_VALID);
    }

    public String notDeleted(){
        return configuration.getConfigurationNode(configurationsConstants.NOT_DELETED);
    }

    public String beneficiariesUsersBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARIES_USERS_BASE_URI);
    }

    public String users(){
        return configuration.getConfigurationNode(configurationsConstants.USERS);
    }

    public String firstName(){
        return configuration.getConfigurationNode(configurationsConstants.FIRST_NAME);
    }

    public String lastName(){
        return configuration.getConfigurationNode(configurationsConstants.LAST_NAME);
    }

    public String phone(){
        return configuration.getConfigurationNode(configurationsConstants.PHONE);
    }

    public String userId(){
        return configuration.getConfigurationNode(configurationsConstants.USER_ID);
    }

    public String marketplacesBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.MARKETPLACES_BASE_URI);
    }

    public String marketplaceswynd(){
        return configuration.getConfigurationNode(configurationsConstants.MARKETPLACE_WIND);
    }

    public String invalidMarketplace(){
        return configuration.getConfigurationNode(configurationsConstants.INVALID_MARKETPLACE);
    }

    public String mongoServer(){
        return configuration.getConfigurationNode(configurationsConstants.MONGO_SERVER);
    }

    public String mongoServerPort(){
        return configuration.getConfigurationNode(configurationsConstants.MONGO_SERVER_PORT);
    }

    public String beneficiariesUsersBaseUriInvalidCreation(){
        return configuration.getConfigurationNode(configurationsConstants.BENEFICIARIES_USERS_BASE_URI_INVALID_CREATION);
    }

    public String validUpdateBeneficiariesProdBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.VALID_UPDATE_BENEFICIARIES_PROD_BASE_URI);
    }

    public String captures(){
        return configuration.getConfigurationNode(configurationsConstants.CAPTURES);
    }

    public String kycStatus(){
        return configuration.getConfigurationNode(configurationsConstants.KYC_STATUS);
    }

    public String kycNotLoaded(){
        return configuration.getConfigurationNode(configurationsConstants.KYC_NOT_LOADED);
    }

    public String kycLoaded(){
        return configuration.getConfigurationNode(configurationsConstants.KYC_LOADED);
    }

    public String kycValidated(){
        return configuration.getConfigurationNode(configurationsConstants.KYC_VALIDATED);
    }

    public String kycRejected(){
        return configuration.getConfigurationNode(configurationsConstants.KYC_REJECTED);
    }

    public String otherValidBeneficiary(){
        return configuration.getConfigurationNode(configurationsConstants.OTHER_VALID_BENEFICIARY);
    }

    public String elasticBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BASE_URI);
    }

    public String elasticBalances(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BALANCES);
    }

    public String elasticSearch(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_SEARCH);
    }

    public String elasticUsername(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_USERNAME);
    }

    public String elasticPassword(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_PASSWORD);
    }

    public String hits(){
        return configuration.getConfigurationNode(configurationsConstants.HITS);
    }

    public String elasticBalanceId(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BALANCE_ID);
    }

    public String elasticSource(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_SOURCE);
    }

    public String elastictotal(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_TOTAL);
    }

    public String elastictotalValue(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_TOTAL_VALUE);
    }

    public String elasticBalancesLimitResults(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BALANCES_LIMIT_RESULTS);
    }

    public String elasticBreakdowns(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BREAKDOWNS);
    }

    public String elasticBreakdownId(){
        return configuration.getConfigurationNode(configurationsConstants.ELASTIC_BREAKDOWN_ID);
    }

    public String internalIntegrationEnvironment(){
        return configuration.getConfigurationNode(configurationsConstants.INTERNAL_INTEGRATION_ENVIRONMENT);
    }

    public String smoneyUsersBaseUri(){
        return configuration.getConfigurationNode(configurationsConstants.SMONEY_USERS_BASE_URI);
    }

    public String smoneyToken(){
        return configuration.getConfigurationNode(configurationsConstants.SMONEY_TOKEN);
    }

    public String address(){
        return configuration.getConfigurationNode(configurationsConstants.ADDRESS);
    }

    public String payoutRulesDefined(){
        return configuration.getConfigurationNode(configurationsConstants.PAYOUT_RULES_DEFINED);
    }

    public String smoneyBeneficiaryProfile(){
        return configuration.getConfigurationNode(configurationsConstants.SMONEY_BENEFICIARY_PROFILE);
    }

    public String smoneyBeneficiaryEmail(){
        return configuration.getConfigurationNode(configurationsConstants.SMONEY_BENEFICIARY_EMAIL);
    }

}

