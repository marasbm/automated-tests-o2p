package configurations;

class ConfigurationsConstants {

    final String ERROR_DESCRIPTION = "errordescription";

    final String ERROR = "error";

    final String AUTHORIZATION = "authorization";

    final String AUTHORIZATION_BASIC = "authorizationbasic";

    final String GRANT_TYPE = "granttype";

    final String CLIENT_CREDENTIALS = "clientcredentials";

    final String INVALID_AUTHORIZATION = "invalidauthorization";

    final String ACCESS_TOKEN = "accesstoken";

    final String TOKEN_TYPE = "tokentype";

    final String OAUTH_TOKEN = "oauthtoken";

    final String INVALID_TOKEN = "invalidtoken";

    final String PAYMENT_PAGES = "paymentpages";

    final String AMOUNT = "amount";

    final String ORDER_ID = "orderid";

    final String MARKET_PLACE_ID = "marketplaceid";

    final String TYPE = "type";

    final String TRANSACTION_ID = "transactionid";

    final String RESPONSE_URL = "responseurl";

    final String CUSTOMER_ID = "customerid";

    final String CURRENCY = "currency";

    final String PAYMENT_PAGE_URL = "paymentpageurl";

    final String RETURN_URL = "returnurl";

    final String PAYMENT_METHOD = "paymentmethod";

    final String SOURCE_CHANNEL = "sourcechannel";

    final String CAPTURE = "capture";

    final String PAYMENT_ID = "paymentid";

    final String BREAKDOWN = "breakdown";

    final String BREAKDOWN_ID = "breakdownid";

    final String TRANSFERS = "transfers";

    final String SUB_ORDER_ID = "suborderid";

    final String BENEFICIARY = "beneficiary";

    final String TRANSFER_ID = "transferid";

    final String MESSAGE = "message";

    final String TRANSACTIONS_CAPTURES = "transactionscaptures";

    final String PAGES = "pages";

    final String PAGES_TOTAL_ELEMENTS = "pagestotalelements";

    final String ITEMS = "items";

    final String AUTHORIZATION_ID = "authorizationid";

    final String CREATION_DATE = "creationdate";

    final String STATUS = "status";

    final String TRANSACTIONS_CAPTURE = "transactionsCapture";

    final String TRANSACTIONS_AUTHORIZATIONS = "transactionsauthorizations";

    final String TRANSACTIONS_AUTHORIZATION = "transactionsauthorization";

    final String INVALID_TRANSACTIONS_AUTHORIZATIONS = "invalidtransactionsauthorizations";

    final String INVALID_TRANSACTIONS_CAPTURES = "invalidtransactionscaptures";

    final String TRANSACTIONS_REFUNDS = "transactionsrefunds";

    final String TRANSACTIONS_REFUND = "transactionsrefund";

    final String INVALID_TRANSACTIONS_REFUND = "invalidtransactionsrefund";

    final String RECEIVER = "receiver";

    final String SENDER = "sender";

    final String ECHOED_MESSAGE = "echoedMessage";

    final String BALANCE_ID = "balanceid";

    final String BALANCES_LIMIT_100 = "balanceslimit100";

    final String TRANSACTIONS_LIST = "transactionsList";

    final String BALANCES_BY_ID = "balancesbyid";

    final String INTEGRATION_ENVIRONMENT = "integrationenvironment";

    final String BALANCES_BASE_URI = "balancesbaseuri";

    final String BREAKDOWNS_BASE_URI = "breakdownsbaseuri";

    final String BENEFICIARY_NOT_ACTIVE = "beneficiarynotactive";

    final String BREAKDOWN_BY_ID = "breakdownbyid";

    final String BENEFICIARIES_BASE_URI = "beneficiariesbaseuri";

    final String BENEFICIARY_ID = "beneficiaryid";

    final String PAYOUT_RULES_BASE_URI = "payoutrulesbaseuri";

    final String VALID_BENEFICIARY = "validbeneficiary";

    final String CRON_EXPRESSION = "cronexpression";

    final String RESERVED_AMOUNT = "reservedamount";

    final String TRANSFERS_HOLDING_DAYS = "transfersholdingDays";

    final String AUTOMATIC_MODE = "automaticmode";

    final String MINIMUM_AMOUNT = "minimumamount";

    final String INVALID_BENEFICIARY = "invalidbeneficiary";

    final String VALID_BENEFICIARY_WITHOUT_PAYOUT_RULES = "validbeneficiarywithoutpayoutrules";

    final String DESCRIPTION = "description";

    final String ACCOUNT_AMOUNT_BASE_URI = "accountamountbaseuri";

    final String BENEFICIARY_TO_UPDATE = "beneficiarytoupdate";

    final String BENEFICIARIES_BACK_OFFICE_UPDATE_STATUS = "beneficiariesbackofficeupdatestatus";

    final String ACTIVATED = "activated";

    final String NOT_ACTIVATED = "notactivated";

    final String DELETED = "deleted";

    final String STATUS_CODE = "statuscode";

    final String EMAIL = "email";

    final String LEGAL_NAME = "legalname";

    final String NOT_VALID = "notvalid";

    final String NOT_DELETED = "notdeleted";

    final String BENEFICIARIES_USERS_BASE_URI = "beneficiariesusersbaseuri";

    final String USERS = "users";

    final String FIRST_NAME = "firstname";

    final String LAST_NAME = "lastname";

    final String PHONE = "phone";

    final String USER_ID = "userid";

    final String MARKETPLACES_BASE_URI = "marketplacesbaseuri";

    final String MARKETPLACE_WIND = "marketplacewynd";

    final String INVALID_MARKETPLACE = "invalidmarketplace";

    final String MONGO_SERVER = "mongoserver";

    final String MONGO_SERVER_PORT = "mongoserverport";

    final String BENEFICIARIES_USERS_BASE_URI_INVALID_CREATION = "beneficiariesusersbaseuriinvalidcreation";

    final String VALID_UPDATE_BENEFICIARIES_PROD_BASE_URI = "validateupdatebeneficiariesprodbaseuri";

    final String CAPTURES = "captures";

    final String KYC_STATUS = "kycstatus";

    final String KYC_NOT_LOADED = "kycnotloaded";

    final String KYC_LOADED = "kycloaded";

    final String KYC_VALIDATED = "kycvalidated";

    final String KYC_REJECTED = "kycrejected";

    final String OTHER_VALID_BENEFICIARY = "othervalidbeneficiary";

    final String ELASTIC_BASE_URI = "elasticbaseuri";

    final String ELASTIC_BALANCES = "elasticbalances";

    final String ELASTIC_SEARCH = "elasticsearch";

    final String ELASTIC_USERNAME = "elasticusername";

    final String ELASTIC_PASSWORD = "elasticpassword";

    final String HITS = "hits";

    final String ELASTIC_BALANCE_ID = "elasticbalanceid";

    final String ELASTIC_SOURCE = "elasticsource";

    final String ELASTIC_TOTAL = "elastictotal";

    final String ELASTIC_TOTAL_VALUE = "elastictotalvalue";

    final String ELASTIC_BALANCES_LIMIT_RESULTS = "elasticbalanceslimitresults";

    final String ELASTIC_BREAKDOWNS = "elasticbrakedowns";

    final String ELASTIC_BREAKDOWN_ID = "elasticbrakedownid";

    final String INTERNAL_INTEGRATION_ENVIRONMENT = "internalintegrationenvironment";

    final String SMONEY_USERS_BASE_URI = "smoneyusersbaseuri";

    final String SMONEY_TOKEN = "smoneytoken";

    final String ADDRESS = "address";

    final String PAYOUT_RULES_DEFINED = "payoutrulesdefined";

    final String SMONEY_BENEFICIARY_PROFILE = "smoneybeneficiaryprofile";

    final String SMONEY_BENEFICIARY_EMAIL = "smoneybeneficiaryemail";
}

