package configurations;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Optional;

class Configuration {

    private Document parsedDocument;

    private Document readParameterInConfigurationFile() {

        try {

            if(parsedDocument == null){

                Optional<String> path = Optional.of(getConfigurationsFullPath());

                File file = new File(path.orElseThrow(()->new NoSuchElementException("ConfigurationsFullPath")));

                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                parsedDocument = documentBuilder.parse(file);
            }

            return parsedDocument;

        } catch (Exception exception) {

            return null;
        }
    }

    private String getConfigurationsFullPath() throws IOException {
        return new File(".").getCanonicalPath() + "/configurations/configuration.xml";
    }

    String getConfigurationNode(String node) {
        return Optional.ofNullable(readParameterInConfigurationFile())
                .map(a -> a.getElementsByTagName(node))
                .map(a -> a.item(0))
                .map(Node::getTextContent).orElseThrow(()->new NoSuchElementException(node));
    }
}
