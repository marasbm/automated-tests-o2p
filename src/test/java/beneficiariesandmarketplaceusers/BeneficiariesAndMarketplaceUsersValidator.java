package beneficiariesandmarketplaceusers;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;


public class BeneficiariesAndMarketplaceUsersValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BeneficiariesAndMarketplaceUsersValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public int getTotalElements(JSONObject jsonObj) {
        return Integer.parseInt(getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString());
    }

    private JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private JSONObject getFirstItemToJSonObject(JSONObject jsonObj) {
        return getItems(jsonObj).getJSONObject(0);
    }

    public String getFirstName(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.firstName()).toString();
    }

    public String getLastName(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.lastName()).toString();
    }

    public String getPhone(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.phone()).toString();
    }

    public String getCreationDate(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.creationDate()).toString();
    }

    public String getUserId(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.userId()).toString();
    }

    public String getEmail(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.email()).toString();
    }

    public String getStatus(JSONObject jsonObj) {
        return getFirstItemToJSonObject(jsonObj).get(configurationsProperties.status()).toString();
    }

    public String getUserByIdFirstName(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.firstName()).toString();
    }

    public String getUserByIdLastName(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.lastName()).toString();
    }

    public String getUserByIdPhone(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.phone()).toString();
    }

    public String getUserByIdCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getUserByIdUserId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.userId()).toString();
    }

    public String getUserByIdEmail(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.email()).toString();
    }

    public String getUserByIdStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }
}