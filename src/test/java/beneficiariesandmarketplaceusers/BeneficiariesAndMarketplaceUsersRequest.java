package beneficiariesandmarketplaceusers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class BeneficiariesAndMarketplaceUsersRequest {

    private ConfigurationsProperties configurationsProperties;

    public BeneficiariesAndMarketplaceUsersRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getUsersFromBeneficiary(String authorizationKey, String authorizationValue, String beneficiary) {
        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.beneficiariesUsersBaseUri().concat("/")
                .concat(beneficiary).concat(configurationsProperties.users()));
    }

    public Response getUserFromBeneficiaryByUserId(String authKey, String authValue, String beneficiary, String userId){
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.beneficiariesUsersBaseUri().concat("/")
                .concat(beneficiary).concat(configurationsProperties.users().concat(userId)));
    }

    public Response getUsersFromMarketplace(String authorizationKey, String authorizationValue, String marketplace){
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.marketplacesBaseUri()
                .concat(marketplace).concat(configurationsProperties.users()));
    }

    public Response getUserFromMarketplaceByUserId(String authKey, String authValue, String marketplace, String userId){
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(configurationsProperties.marketplacesBaseUri()
                .concat(marketplace).concat(configurationsProperties.users().concat(userId)));
    }

    public Response createBeneficiaryUser(String authKey, String authValue, String beneficiary, String requestBody){
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.beneficiariesUsersBaseUri().concat("/")
                .concat(beneficiary).concat(configurationsProperties.users()));
    }

    public Response invalidCreationBeneficiaryUser(String authKey, String authValue, String beneficiary, String requestBody){
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.beneficiariesUsersBaseUriInvalidCreation()
                .concat(beneficiary).concat(configurationsProperties.users()));
    }

    public Response invalidCreationUser(String authKey, String authValue, String requestBody, String endpoint){
        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(endpoint);
    }

    public Response createMarketPlaceUser(String authKey, String authValue, String marketplace, String requestBody){
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authKey, authValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post(configurationsProperties.marketplacesBaseUri()
                .concat(marketplace).concat(configurationsProperties.users()));
    }

    public String createUserRequestBody(String email){
        return "{\n" +
                "\"firstName\":\"VALLE\",\n" +
                "\"lastName\":\"DA\",\n" +
                "\"email\":\""+email+"\",\n" +
                "\"phone\":\"132456\"\n" +
                "}";
    }

    public String createUserRequestBodyWithAlreadyExistsId(String email, String userId){
        return "{\n" +
                "\"userId\":\""+userId+"\",\n" +
                "\"firstName\":\"Daniel\",\n" +
                "\"lastName\":\"Valle\",\n" +
                "\"email\":\""+email+"\",\n" +
                "\"phone\":\"132456\"\n" +
                "}";
    }
}
