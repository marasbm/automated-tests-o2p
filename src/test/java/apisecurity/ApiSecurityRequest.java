package apisecurity;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import static com.jayway.restassured.RestAssured.given;

public class ApiSecurityRequest {

    public static Response getNotAuthorizedClientNoHttpHeaderAuthorization(String baseUri) {

        RestAssured.baseURI = baseUri;

        RequestSpecification httpRequest = given();

        return httpRequest.get();
    }

    public static Response getMasterPayApiAuthorizationClient(String baseUri, String authorizationKey,
                                                              String authorizationValue, ContentType contentType) {
        RestAssured.baseURI = baseUri;

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(contentType);

        return httpRequest.get();
    }
}
