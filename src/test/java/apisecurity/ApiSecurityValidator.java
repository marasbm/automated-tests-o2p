package apisecurity;

import configurations.ConfigurationsProperties;
import org.json.JSONObject;

public class ApiSecurityValidator {

    private final ConfigurationsProperties configurationsProperties;

    public ApiSecurityValidator(ConfigurationsProperties _configurationsProperties){

        configurationsProperties = _configurationsProperties;
    }

    public String getErrorDescription(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.errorDescription()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }
}
