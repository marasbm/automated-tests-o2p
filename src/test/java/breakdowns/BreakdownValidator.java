package breakdowns;

import configurations.ConfigurationsProperties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class BreakdownValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BreakdownValidator() {

        configurationsProperties = new ConfigurationsProperties();
    }

    public String getCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getBreakdownId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.breakdownId()).toString();
    }

    public String getTransactionId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.transactionId()).toString();
    }

    private JSONArray getTransfers(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.transfers());
    }

    private List<JSONObject> getTransfersToJSonObject(JSONObject jsonObj) {
        List<JSONObject> transfersList = new ArrayList<>();

        for (int i = 0; i < getTransfers(jsonObj).length(); i++) {
            JSONObject object = getTransfers(jsonObj).getJSONObject(i);
            transfersList.add(object);
        }

        return transfersList;
    }

    private List<String> transfersAmount(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.amount()
        ).toString()).collect(Collectors.toList());
    }

    public String getTransfersBeneficiaryId(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.beneficiary()
        ).toString()).findFirst().orElseThrow(()-> new NoSuchElementException("Beneficiary not found"));
    }

    private List<String> transfersTransferId(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.transferId()
        ).toString()).collect(Collectors.toList());
    }

    public boolean validateTransfersSubOrderIdNotEmpty(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.subOrderId()).toString()));
    }

    public boolean validateAllTransferIdNotEquals(JSONObject jsonObj){
        return transfersTransferId(jsonObj).size() ==  transfersTransferId(jsonObj).stream().distinct().count();
    }

    public boolean validateAllTransfersAmountGreaterThanZero(JSONObject jsonObj){
        return transfersAmount(jsonObj).stream().allMatch(x -> Integer.parseInt(x) > 0);
    }

    public boolean validateTransfersBeneficiaryNotEmpty(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.beneficiary()).toString()));
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public String getTotalElements(JSONObject jsonObj) {
        return getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString();
    }

    private JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getItems(jsonObj).length(); i++) {
            JSONObject object = getItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public List<String> getItemsCreationDateList(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.creationDate()
        ).toString()).collect(Collectors.toList());
    }

    public boolean validateItemsBreakdownIdNotEmpty(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.breakdownId()).toString()));
    }

    public boolean validateItemsTransactionIdNotEmpty(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.transactionId()).toString()));
    }

    public boolean validateItemsStatusIdNotEmpty(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.status()).toString()));
    }

    private List<JSONArray> getItemsTransfersJSONArray(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().map(x->(JSONArray)x.get(configurationsProperties.transfers()))
                .collect(Collectors.toList());
    }

    private JSONObject getItemsTransfersJSONObject(JSONObject jsonObj){
        return getItemsTransfersJSONArray(jsonObj).stream().findFirst()
                .orElseThrow(()-> new NoSuchElementException("Items transfers not found")).getJSONObject(0);
    }

    public String itemsTransfersAmount(JSONObject jsonObj){
        return getItemsTransfersJSONObject(jsonObj).get(configurationsProperties.amount()).toString();
    }

    public String itemsTransfersSubOrderId(JSONObject jsonObj){
        return getItemsTransfersJSONObject(jsonObj).get(configurationsProperties.subOrderId()).toString();
    }

    public String itemsTransfersBeneficiary(JSONObject jsonObj){
        return getItemsTransfersJSONObject(jsonObj).get(configurationsProperties.beneficiary()).toString();
    }

    public String itemsTransfersCreationDate(JSONObject jsonObj){
        return getItemsTransfersJSONObject(jsonObj).get(configurationsProperties.creationDate()).toString();
    }

    public String itemsTransfersTransferId(JSONObject jsonObj){
        return getItemsTransfersJSONObject(jsonObj).get(configurationsProperties.transferId()).toString();
    }
}