package breakdowns;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class BreakdownRequest {

    private ConfigurationsProperties configurationsProperties;

    public BreakdownRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response postBreakdownRequest(String authorizationKey, String authorizationValue, String requestBody) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment().concat(
                configurationsProperties.breakdownsBaseUri());

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestBody);

        return httpRequest.post();
    }

    public Response getBreakdownsRequest(String authorizationKey, String authorizationValue, String queryString) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }

    public String postBreakdownRequestBody() {
        return "{\n" +
                "\"transactionId\":\"f285034d-3655-425f-9a08-b055f09d85da\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"1551952366864000\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"1551952366864000\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyWithNoRequiredFields() {
        return "{\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyWithNoRequiredFieldAmount() {
        return "{\n" +
                "\"transactionId\":\"6b91d0cb-21e4-4d2d-9331-4aaf407a51c9\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyNullRequiredFieldAmount() {
        return "{\n" +
                "\"transactionId\":\"6b91d0cb-21e4-4d2d-9331-4aaf407a51c9\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"null\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyNotValidRequiredFieldAmount() {
        return "{\n" +
                "\"transactionId\":\"6b91d0cb-21e4-4d2d-9331-4aaf407a51c9\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"011\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyNotValidRequiredFieldTransactionId() {
        return "{\n" +
                "\"transactionId\":\"6b91d0cb-21e4-4d2d-9331-4aabvcdf\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"10000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyWithNoRequiredFieldTransactionId() {
        return "{\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"100000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"1000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }

    public String postBreakdownRequestBodyNotValidRequiredFieldBeneficiary() {
        return "{\n" +
                "\"transactionId\":\"6b91d0cb-21e4-4d2d-9331-4aaf407a51c9\",\n" +
                "\"transfers\":[\n" +
                "{\n" +
                "\"amount\":\"011\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"0123456\"\n" +
                "},\n" +
                "{\n" +
                "\"amount\":\"5000000\",\n" +
                "\"subOrderId\":\"123\",\n" +
                "\"beneficiary\":\"123456\"\n" +
                "}\n" +
                "],\n" +
                "\"echoedMessage\":\"ECHOES FROM THE PAST...\"\n" +
                "}";
    }
}