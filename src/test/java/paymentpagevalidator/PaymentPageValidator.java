package paymentpagevalidator;

import configurations.ConfigurationsProperties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PaymentPageValidator {

    private static ConfigurationsProperties configurationsProperties;

    public PaymentPageValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public String getAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getOrderId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.orderId()).toString();
    }

    public String getMarketPlaceId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.marketPlaceId()).toString();
    }

    public String getType(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.type()).toString();
    }

    public String getTransactionId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.transactionId()).toString();
    }

    public String getResponseUrl(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.responseUrl()).toString();
    }

    public String getCustomerId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.customerId()).toString();
    }

    public String getCurrency(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.currency()).toString();
    }

    public String getPaymentPageUrl(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.paymentPageUrl()).toString();
    }

    public String getReturnUrl(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.returnUrl()).toString();
    }

    public String getPaymentMethod(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.paymentMethod()).toString();
    }

    private JSONObject getCapture(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.capture());
    }

    public String getCaptureAmount(JSONObject jsonObj) {
        return getCapture(jsonObj).get(configurationsProperties.amount()).toString();
    }

    public String getPaymentId(JSONObject jsonObj) {
        return getCapture(jsonObj).get(configurationsProperties.paymentId()).toString();
    }

    private JSONObject getBreakdown(JSONObject jsonObj) {
        return (JSONObject) getCapture(jsonObj).get(configurationsProperties.breakdown());
    }

    public String getBreakdownId(JSONObject jsonObj) {
        return getBreakdown(jsonObj).get(configurationsProperties.breakdownId()).toString();
    }

    public String getBreakdownTransactionId(JSONObject jsonObj) {
        return getBreakdown(jsonObj).get(configurationsProperties.transactionId()).toString();
    }

    private JSONArray getTransfers(JSONObject jsonObj) {
        return (JSONArray) getBreakdown(jsonObj).get(configurationsProperties.transfers());
    }

    private List<JSONObject> getTransfersToJSonObject(JSONObject jsonObj) {
        List<JSONObject> transfersList = new ArrayList<>();

        for (int i = 0; i < getTransfers(jsonObj).length(); i++) {
            JSONObject object = getTransfers(jsonObj).getJSONObject(i);
            transfersList.add(object);
        }

        return transfersList;
    }

    private List<String> transfersAmount(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.amount()
        ).toString()).collect(Collectors.toList());
    }

    private List<String> transfersTransferId(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.transferId()
        ).toString()).collect(Collectors.toList());
    }

    public int transfersTotalAmount(JSONObject jsonObj){
        int result =0;
        for (int i = 0; i < transfersAmount(jsonObj).size(); i++) {
            result += Integer.parseInt(transfersAmount(jsonObj).get(i));
        }

        return result;
    }

    public boolean validateTransfersSubOrderIdNotEmpty(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.subOrderId()).toString()));
    }

    public boolean validateBeneficiaryNotEmpty(JSONObject jsonObj){
        return getTransfersToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.beneficiary()).toString()));
    }

    public boolean validateAllTransferIdNotEquals(JSONObject jsonObj){
        return transfersTransferId(jsonObj).size() ==  transfersTransferId(jsonObj).stream().distinct().count();
    }

    public String getMessage(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    public String getDescription(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.description()).toString();
    }
}