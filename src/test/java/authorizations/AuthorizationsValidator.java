package authorizations;

import configurations.ConfigurationsProperties;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorizationsValidator {

    private static ConfigurationsProperties configurationsProperties;

    public AuthorizationsValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getPages(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.pages());
    }

    public String getTotalElements(JSONObject jsonObj) {
        return getPages(jsonObj).get(configurationsProperties.pagesTotalElements()).toString();
    }

    public JSONArray getItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getItems(jsonObj).length(); i++) {
            JSONObject object = getItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public boolean getAmount(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.amount()).toString()));
    }

    public boolean getAuthorizationId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.authorizationId()).toString()));
    }

    public boolean getCreationDate(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.creationDate()).toString()));
    }

    public List<String> getCreationDateList(JSONObject jsonObj){
        return getItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.creationDate()
        ).toString()).collect(Collectors.toList());
    }

    public boolean getStatus(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.status()).toString()));
    }

    public boolean getTransactionId(JSONObject jsonObj) {
        return getItemsToJSonObject(jsonObj).stream().allMatch(x-> StringUtils.isNotEmpty(
                x.get(configurationsProperties.transactionId()).toString()));
    }

    public String getAuthorizationAmount(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.amount()).toString();
    }

    public String getAuthorizationAuthorizationId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.authorizationId()).toString();
    }

    public String getAuthorizationCreationDate(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.creationDate()).toString();
    }

    public String getAuthorizationStatus(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.status()).toString();
    }

    public String getAuthorizationTransactionId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.transactionId()).toString();
    }

    public String getError(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.error()).toString();
    }
}