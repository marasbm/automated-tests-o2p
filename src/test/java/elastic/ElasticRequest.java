package elastic;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class ElasticRequest {

    private ConfigurationsProperties configurationsProperties;

    public ElasticRequest(){
        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getElasticBalancesRequest() {
        RestAssured.baseURI = configurationsProperties.elasticBaseUri().concat(
                configurationsProperties.elasticBalances().concat(
                        configurationsProperties.elasticSearch()));

        RequestSpecification httpRequest = given().auth().basic(
                configurationsProperties.elasticUsername(), configurationsProperties.elasticPassword());

        httpRequest.contentType(ContentType.JSON);

        String limitResults = "?size="+configurationsProperties.elasticBalancesLimitResults()+"&pretty=true";

        return httpRequest.get(limitResults);
    }

    public Response getElasticBreakdownsRequest() {
        RestAssured.baseURI = configurationsProperties.elasticBaseUri().concat(
                configurationsProperties.elasticBreakdowns().concat(
                        configurationsProperties.elasticSearch()));

        RequestSpecification httpRequest = given().auth().basic(
                configurationsProperties.elasticUsername(), configurationsProperties.elasticPassword());

        httpRequest.contentType(ContentType.JSON);

        String limitResults = "?size="+configurationsProperties.elasticBalancesLimitResults()+"&pretty=true";

        return httpRequest.get(limitResults);
    }
}