package elastic;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ElasticValidator {

    private static ConfigurationsProperties configurationsProperties;

    public ElasticValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONObject getHits(JSONObject jsonObj) {
        return (JSONObject) jsonObj.get(configurationsProperties.hits());
    }

    private JSONObject getHitsTotal(JSONObject jsonObj) {
        return (JSONObject)getHits(jsonObj).get(configurationsProperties.elastictotal());
    }

    public String getHitsTotalValue(JSONObject jsonObj){
        return getHitsTotal(jsonObj).get(configurationsProperties.elastictotalValue()).toString();
    }

    private JSONArray getHitsValue(JSONObject jsonObj) {
        return getHits(jsonObj).getJSONArray(configurationsProperties.hits());
    }

    public List<JSONObject> getHitsValueToJsonObject(JSONObject jsonObj) {
        List<JSONObject> getHitsList = new ArrayList<>();

        for (int i = 0; i < getHitsValue(jsonObj).length(); i++) {
            JSONObject object = getHitsValue(jsonObj).getJSONObject(i);
            getHitsList.add(object);
        }

        return getHitsList;
    }

    private JSONObject getHitsFromCreatedBalance(JSONObject jsonObj, String balanceId){
        return getHitsValueToJsonObject(jsonObj).stream()
                .filter(x->x.get(configurationsProperties.elasticBalanceId()).equals(balanceId))
                .findFirst().orElseThrow(()-> new NoSuchElementException("balanceId created not found"));
    }

    private JSONObject getHitsFromCreatedBreakdown(JSONObject jsonObj, String breakdownId){
        return getHitsValueToJsonObject(jsonObj).stream()
                .filter(x->x.get(configurationsProperties.elasticBreakdownId()).equals(breakdownId))
                .findFirst().orElseThrow(()-> new NoSuchElementException("breakdownId created not found"));
    }

    private JSONObject elasticGetBalanceSource(JSONObject jsonObj, String balanceId){
        return (JSONObject)getHitsFromCreatedBalance(jsonObj,balanceId).get(configurationsProperties.elasticSource());
    }

    private JSONObject elasticGetBreakdownSource(JSONObject jsonObj, String breakdownId){
        return (JSONObject)getHitsFromCreatedBreakdown(jsonObj,breakdownId).get(configurationsProperties.elasticSource());
    }

    public String elasticGetBalanceId(JSONObject jsonObj, String balanceId){
        return getHitsFromCreatedBalance(jsonObj,balanceId).get(configurationsProperties.elasticBalanceId()).toString();
    }

    public String elasticGetAmount(JSONObject jsonObj, String balanceId){
        return elasticGetBalanceSource(jsonObj,balanceId).get(configurationsProperties.amount()).toString();
    }

    public String elasticGetSender(JSONObject jsonObj, String balanceId){
        return elasticGetBalanceSource(jsonObj,balanceId).get(configurationsProperties.sender()).toString();
    }

    public String elasticGetReceiver(JSONObject jsonObj, String balanceId){
        return elasticGetBalanceSource(jsonObj,balanceId).get(configurationsProperties.receiver()).toString();
    }

    public String elasticGetBreakdownId(JSONObject jsonObj, String breakdownId){
        return elasticGetBreakdownSource(jsonObj,breakdownId).get(configurationsProperties.breakdownId()).toString();
    }

    public String elasticGetTransactionId(JSONObject jsonObj, String breakdownId){
        return elasticGetBreakdownSource(jsonObj,breakdownId).get(configurationsProperties.transactionId()).toString();
    }

    public JSONArray elasticGetTransfers(JSONObject jsonObj, String breakdownId) {
        return elasticGetBreakdownSource(jsonObj,breakdownId).getJSONArray(configurationsProperties.transfers());
    }
}