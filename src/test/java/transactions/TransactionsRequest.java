package transactions;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;

import static com.jayway.restassured.RestAssured.given;

public class TransactionsRequest {

    private ConfigurationsProperties configurationsProperties;

    public TransactionsRequest(){

        configurationsProperties = new ConfigurationsProperties();
    }

    public Response getTransactionsRequest(String authorizationKey, String authorizationValue, String queryString) {
        RestAssured.baseURI = configurationsProperties.integrationEnvironment();

        RequestSpecification httpRequest = given();

        httpRequest.header(authorizationKey, authorizationValue);

        httpRequest.contentType(ContentType.JSON);

        return httpRequest.get(queryString);
    }
}