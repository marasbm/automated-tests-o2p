package transactions;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionsValidator {

    private static ConfigurationsProperties configurationsProperties;

    public TransactionsValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    private JSONArray getTransactionsItems(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.items());
    }

    private List<JSONObject> getTransactionsItemsToJSonObject(JSONObject jsonObj) {
        List<JSONObject> itemsList = new ArrayList<>();

        for (int i = 0; i < getTransactionsItems(jsonObj).length(); i++) {
            JSONObject object = getTransactionsItems(jsonObj).getJSONObject(i);
            itemsList.add(object);
        }

        return itemsList;
    }

    public List<String> getTransactionsIds(JSONObject jsonObj){
        return getTransactionsItemsToJSonObject(jsonObj).stream().map(x->x.get(configurationsProperties.transactionId()
        ).toString()).collect(Collectors.toList());
    }

    public String getTransactionId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.transactionId()).toString();
    }

    public JSONObject getFirstTransactionToJSonObject(JSONObject jsonObj) {
        return getTransactionsItems(jsonObj).getJSONObject(0);
    }

    public String getOrderId(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.orderId()).toString();
    }

    private JSONArray getTransactionCaptures(JSONObject jsonObj) {
        return (JSONArray) jsonObj.get(configurationsProperties.captures());
    }

    private JSONObject getTransactionCapturesToJSonObject(JSONObject jsonObject) {
        return getTransactionCaptures(jsonObject).getJSONObject(0);
    }

    public String getTransactionCapturesPaymentId(JSONObject jsonObject) {
        return getTransactionCapturesToJSonObject(jsonObject).get(configurationsProperties.paymentId()).toString();
    }

    public String getFirstTransactionId(JSONObject jsonObj) {
        return getFirstTransactionToJSonObject(jsonObj).get(configurationsProperties.transactionId()).toString();
    }
}